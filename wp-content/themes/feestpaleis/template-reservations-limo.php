<?php
/*
Template name: Reservations Limo
*/
?>

<?php get_header(); ?>

<div class="main">
	<div class="container container-padding">

		<div class="half_column_last reservations-content">

			<div class="reservations-thumb">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/reservation_vip_tafel.jpg" alt="" />
				<ul>
					<li><a href="<?php echo get_permalink(15); ?>">VIP Tafel</a></li>
					<li><a href="<?php echo get_permalink(391); ?>" class="active">Limo service</a></li>
					<li><a href="<?php echo get_permalink(450); ?>">Groepen</a></li>
				</ul>
			</div>

			<div class="reservations-container">

				<div class="reservations-container-inner">

					<h5 class="color-main">Limo service</h5>
				    <?php
				    while ( have_posts() ) : the_post(); ?> 
			            <?php the_content(); ?> 
				    <?php
				    endwhile; 
				    wp_reset_query(); 
				    ?>

					<div class="reservations-options">
						<?php echo do_shortcode('[contact-form-7 id="396" title="Contactform Limo"]'); ?>
					</div>

				</div>

			</div>

		</div>

		<div class="half_column">

			<div class="main-events blocky">

				<?php
					$now = (date('U') - 86400) * 1000;
				$coming_events_args = array(
					'post_type' => 'event',
					'posts_per_page' => 3,
			            'meta_key' => 'event_date',
			            'meta_query' => array(
							array(
								'key'     => 'event_date',
								'value'   => $now,
								'compare' => '>='
							)
						),
			            'orderby' => 'meta_value',
			            'order' => 'ASC'
				);

				$coming_events = new WP_Query( $coming_events_args );

				if( $coming_events->have_posts() ) :
					while( $coming_events->have_posts() ) :
						$coming_events->the_post();
					
						get_template_part('content', 'event');

					endwhile;
				endif;
				?>

			</div>

			<div class="concept-nav-small">
					
				<?php 

				$event_cat_args = array(
				    'orderby'           => 'menu_order', 
				    'order'             => 'ASC',
				    'hide_empty'        => false, 
				    'exclude'           => array(), 
				    'exclude_tree'      => array(), 
				    'include'           => array(),
				    'number'            => '', 
				    'fields'            => 'all', 
				    'slug'              => '', 
				    'parent'            => '',
				    'hierarchical'      => true, 
				    'child_of'          => 0, 
				    'get'               => '', 
				    'name__like'        => '',
				    'description__like' => '',
				    'pad_counts'        => false, 
				    'offset'            => '', 
				    'search'            => '', 
				    'cache_domain'      => 'core'
				); 

				$event_cats = get_terms('event_concepts', $event_cat_args);

				if( $event_cats ) :
					?><ul class="main-concept-list-big"><?php

					foreach( $event_cats as $cat ) :

						$term_link = get_term_link( $cat );

						?><li><?php

							if( $cat->slug == 'i-love-feestpaleis' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept ilfp">
										<div class="event-concept-inner">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_ilfp.png" alt="i<3FeestPaleis" />
										</div>
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							if( $cat->slug == 'lovely-sundays' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept ls">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_ls.png" alt="Lovely Sundays" />
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							if( $cat->slug == 'after-work' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept aw">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_aw.png" alt="After Work" />
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							if( $cat->slug == 'level-3' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept l3">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_l3.png" alt="Lever 3" />
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							?></li><?php

					endforeach;

					?></ul><?php
				endif;

				?>

			</div>

			<div class="main-social">
			
				<?php get_template_part('content', 'social_list'); ?>

			</div>
			
			<div class="side-gallery">
				<?php

				$gallery_images = get_field('home_gallery', 39);
				if( $gallery_images ): ?>
					<ul class="side-gallery-grid-full">
					<?php $gallery_counter = 0; ?>
					<?php foreach( $gallery_images as $img ): ?>
						<li>
							<a class="fancybox" rel="home_gallery" href="<?php echo $img['url']; ?>">
								<img src="<?php echo $img['sizes']['thumbnail']; ?>" alt="<?php echo $img['alt']; ?>" />
								<span class="main-gallery-overlay"></span>
							</a>
						</li>
						<?php if (++$gallery_counter == 12) break; ?>
					<?php endforeach; ?>
				</ul>
				<?php endif;

				?>
			</div>

		</div>

	</div>
</div>

<?php get_footer(); ?>