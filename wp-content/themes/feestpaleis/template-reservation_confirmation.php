<?php
/*
Template name: Reservations Confirmation
*/
?>

<?php 

get_header(); ?>

<div class="main">
	<div class="container container-padding">

		<div class="half_column_last reservations-content">
			
			<div class="reservations-thumb">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/reservation_vip_tafel.jpg" alt="" />
				<ul>
					<li><a href="<?php echo get_permalink(15); ?>" class="active">VIP Tafel</a></li>
					<li><a href="<?php echo get_permalink(391); ?>">Limo service</a></li>
					<li><a href="<?php echo get_permalink(450); ?>">Groepen</a></li>
				</ul>
			</div>

			<div class="reservations-container">

				<div class="reservations-container-inner">

					<h5 class="color-main">Bedankt voor uw reservatie</h5>
				    <?php
				    while ( have_posts() ) : the_post(); ?> 
			            <?php the_content(); ?> 
				    <?php
				    endwhile; 
				    wp_reset_query(); 
				    ?>

					<div class="reservations-checkout">

						<p>&nbsp;</p>
						<h5>Shuttle service</h5>
						<p><a href="<?php echo get_permalink(394); ?>" style="color:#d5004b;">Reserveer onze shuttle</a>, privé, vanaf 6 tot 8 personen</p>
						<p>&nbsp;</p>
						<h5>Limo service</h5>
						<p><a href="<?php echo get_permalink(394); ?>" style="color:#d5004b;">Reserveer een limousine</a>, toegang voor 8 personen, vanaf €295,00</p>
					</div>

				</div>

			</div>

		</div>

		<div class="half_column">

			<div class="main-events">

				<?php
					$now = (date('U') - 86400) * 1000;
				$coming_events_args = array(
					'post_type' => 'event',
					'posts_per_page' => 3,
			            'meta_key' => 'event_date',
			            'meta_query' => array(
							array(
								'key'     => 'event_date',
								'value'   => $now,
								'compare' => '>='
							)
						),
			            'orderby' => 'meta_value',
			            'order' => 'ASC'
				);

				$coming_events = new WP_Query( $coming_events_args );

				if( $coming_events->have_posts() ) :
					while( $coming_events->have_posts() ) :
						$coming_events->the_post();
					
						get_template_part('content', 'event');

					endwhile;
				endif;
				?>

			</div>

			<div class="main-social">
			
				<?php get_template_part('content', 'social_list'); ?>

			</div>

		</div>

	</div>
</div>

<?php get_footer(); ?>