<div class="footer">

	<div class="wrappy-white">
	
		<div class="social">
			<div class="container container-padding">

				<h3>Be social, tag us with <span class="bold">#FEESTPALEIS</span></h3>
				
				<div class="instagram">

					<div class="instagram-mask"><a href="http://www.instagram.com" target="_blank">&nbsp;</a></div>

					<ul id="instafeed">
					</ul>
					
				</div>

				<div class="twitter">

					<div class="twitter-mask"></div>

					<div class="tweets">
						
					</div>
					
				</div>

				<div class="facebook">
					<div style="margin: -1px -1px -1px -1px;">

					
						<div class="fb-like-box" data-href="https://www.facebook.com/feestpaleis" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="true" data-height="300px"></div>

					</div>
				</div>
			
			</div>
		</div>

		<div class="newsletter">
			<div class="container clearfix">

				<h3>Get your free entrance ticket <span class="bold">subscribe now!</span></h3>
			
				<div class="newsletter-optin">

					<?php echo do_shortcode('[mc4wp_form]'); ?>
					<p class="subtile">
						* you will receive your unique entrance-ticket by email.
					</p>

				</div>

			</div>
		</div>
		
	</div>

	<div class="foot">
		<div class="container container-padding">

			<div class="foot-logo">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/logo_fp_footer.png" alt="Feestpaleis" class="no-resp" />
			</div>
		
			<div class="foot-note">

				<div class="foot-contact">

					<h5>Contact</h5>
					<p>Danstheater Feestpaleis<br />
						Toleindestraat 20<br />
						9080 Beervelde
					</p>
					
					<div class="foot-contacts">
						<p>
							<span class="fixed_w">T:</span>+32 (0) 9 355 52 39
						</p>
						<p>
							<span class="fixed_w">F:</span>+32 (0) 9 355 23 53</p>
						<p>
							<span class="fixed_w">E:</span><a href="mailto:info@feestpaleis.be">info@feestpaleis.be</a>
						</p>
					</div>

				</div>

				<div class="foot-nav">

					<h5>More</h5>

					<?php

					$footnav_args = array(
						'theme_location'  => 'footnav',
						'menu'            => '',
						'container'       => '',
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => 'footnav',
						'menu_id'         => '',
						'echo'            => true,
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'depth'           => 0,
						'walker'          => ''
					);

					wp_nav_menu( $footnav_args );

					?>

				</div>

			</div>

		</div>

	</div>

</div>

<?php wp_footer(); ?>
</body>
</html>