<?php $event_term = wp_get_post_terms( $post->ID, 'event_concepts' ); ?>

<?php
//var_dump($event_term);
if( $event_term[0]->term_id == 4 ) : // ILFP

?><div class="main-event ilfp-event"><?php

elseif( $event_term[0]->term_id == 5 ) : // LS

?><div class="main-event ls-event"><?php

elseif( $event_term[0]->term_id == 6 ) : // AW

?><div class="main-event aw-event"><?php

elseif( $event_term[0]->term_id == 8 ) : // L3

?><div class="main-event l3-event"><?php

else :

?><div class="main-event"><?php

endif;
?>

	<a href="<?php the_permalink(); ?>" class="big-button">

	<div class="main-event-date">

	<?php
	$epoch = (get_field('event_date') / 1000) + 7200;
	$event_date = DateTime::createFromFormat('U', $epoch);
	?>

		<div class="main-event-row">
			<div class="main-event-day">
				<?php if($event_date) : echo $event_date->format('d'); endif; ?>
			</div>

			<div class="main-event-date_wrapper">
				<div class="main-event-month">
					<?php if($event_date) : echo $event_date->format('M'); endif; ?>
				</div>
				<div class="main-event-year">
					<?php if($event_date) : echo $event_date->format('Y'); endif; ?>
				</div>
			</div>
		</div>

	</div>

	<div class="main-event-meta">

		<div class="main-event-title">
			<h2>
				<?php the_title(); ?>
			</h2>
		</div>

	</div>

	</a>

	<!-- <div class="main-event-rsvp"> -->
	<a href="<?php the_field('event_fb_link'); ?>" class="rsvp" target="_blank">Facebook Event</a>
	<!-- </div> -->

</div>