<?php get_header(); ?>

<div class="main">
	<div class="container container-padding">

		<div class="row">

			<div class="half_column_last">

				<div class="event-full">

					<?php if( have_posts() ) :
						while( have_posts() ) :
							the_post() ;
							?>

							<?php
							$epoch = (get_field('event_date') / 1000) + 7200;
							$event_date = DateTime::createFromFormat('U', $epoch);
							?>
							
							<?php if ( has_post_thumbnail() ) { ?>
							<div class="event-full-image">
								<?php the_post_thumbnail( 'large' ); ?>
							</div>
							<? } ?>

							<div class="event-full-meta">
								
								<div class="event-full-meta-head">
									<div class="event-full-meta-left">
										<h1><?php the_title(); ?></h1>
										<p class="event-full-meta-date"><?php echo $event_date->format('d M y'); ?></p>
									</div>
									<div class="event-full-meta-right">
										<ul class="event-info-icons">
											<li><div class="event-info-icon event-info-icon-price" title="<?php echo '&euro;' . get_field('event_price'); ?>" rel="tooltip"></div></li>
											<li><div class="event-info-icon event-info-icon-dresscode" title="<?php echo get_field('event_dresscode'); ?>" rel="tooltip"></div></li>
											<li><div class="event-info-icon event-info-icon-age" <?php if(get_field('event_age_desc')) { ?> title="<?php echo get_field('event_age_desc'); ?>" <?php } else { ?> title="<?php echo 'Leeftijd ' . get_field('event_age'); ?>+" <? } ?> rel="tooltip"><?php echo get_field('event_age'); ?>+</div></li>
										</ul>
									</div>
								</div>

								<div class="event-full-meta-body">
									<?php the_content(); ?>
								</div>

								<div class="event-full-meta-cta">
									<a href="<?php the_field('event_fb_link'); ?>" class="btn btn-wide" target="_blank">Facebook event</a>
								</div>

							</div>

							<?php
						endwhile;
					endif; ?>

				</div>

			</div>

			<div class="half_column">

				<div class="main-events">

				<?php

				$curID = get_the_ID();
				
				$now = (date('U') - 86400) * 1000;
					$coming_events_args = array(
						'post_type' => 'event',
						'posts_per_page' => 6,
			            'meta_key' => 'event_date',
			            'meta_query' => array(
							array(
								'key'     => 'event_date',
								'value'   => $now,
								'compare' => '>='
							)
						),
			            'orderby' => 'meta_value',
			            'order' => 'ASC'
					);

				$coming_events = new WP_Query( $coming_events_args );

				if( $coming_events->have_posts() ) :
					while( $coming_events->have_posts() ) :
						$coming_events->the_post();

						$event_term = wp_get_post_terms( $post->ID, 'event_concepts' );
						
						?>

						<?php
						if( $event_term[0]->term_id == 4 ) : // ILFP

							?><div class="main-event ilfp-event"><?php

						elseif( $event_term[0]->term_id == 5 ) : // LS

							?><div class="main-event ls-event"><?php

						elseif( $event_term[0]->term_id == 6 ) : // AW

							?><div class="main-event aw-event"><?php

						elseif( $event_term[0]->term_id == 8 ) : // L3

							?><div class="main-event l3-event"><?php

						else :

							?><div class="main-event"><?php

						endif;
						?>

							<a href="<?php the_permalink(); ?>" class="big-button">

								<div class="main-event-date">

									<?php
									$epoch = (get_field('event_date') / 1000) + 7200;
									$event_date = DateTime::createFromFormat('U', $epoch);
									?>
								
									<div class="main-event-row">
										<!--<div class="main-event-day <?php if( $curID == $post->ID) : echo "main-event-current"; endif; ?>">-->
										<div class="main-event-day">
											<?php echo $event_date->format('d'); ?>
										</div>

										<div class="main-event-date_wrapper">
											<div class="main-event-month">
												<?php echo $event_date->format('M'); ?>
											</div>
											<div class="main-event-year">
												<?php echo $event_date->format('Y'); ?>
											</div>
										</div>

									</div>

								</div>

								<div class="main-event-meta">
									
									<div class="main-event-title">
										<h2>
											<?php the_title(); ?>
										</h2>
									</div>

								</div>

							</a>

							<!-- <div class="main-event-rsvp"> -->
							<a href="<?php the_field('event_fb_link'); ?>" class="rsvp" target="_blank">Facebook Event</a>
							<!-- </div> -->

							<?php
							if( $curID == $post->ID) :
								?>
								<span class="current-indicator">
								</span>
								<?php
							endif;
							?>

							</div>

						<?php
						endwhile;
					endif;

					wp_reset_query();

					?>

				</div>

				<div class="main-social">
			
					<?php get_template_part('content', 'social_list'); ?>

				</div>

			</div>

		</div>

	</div>
</div>

<?php get_footer(); ?>