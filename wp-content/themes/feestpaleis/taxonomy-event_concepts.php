<?php get_header(); ?>

<div class="main">
	<div class="container container-padding">

		<div class="row">

			<div class="half_column_last concept-content">

				<?php /* if( have_posts() ) :
					while( have_posts() ) :
						the_post();
						*/ ?>

						<div class="concept-header">

							<div class="concept-branding">

								<div class="concept-logo">
									<?php
									if( get_queried_object()->term_id == 4 ) :
										?><div class="event-concept">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_ilfp.png" alt="i<3FeestPaleis" />
										</div><?php
									endif;

									if( get_queried_object()->term_id == 5 ) :
										?><div class="event-concept">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_ls.png" alt="Lovely Sundays" />
										</div><?php
									endif;

									if( get_queried_object()->term_id == 6 ) :
										?><div class="event-concept">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_aw.png" alt="After Work" />
										</div><?php
									endif;

									if( get_queried_object()->term_id == 8 ) :
										?><div class="event-concept">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_l3.png" alt="Lever 3" />
										</div><?php
									endif;
									?>
								</div>

								<div class="concept-meta">
									<span class="concept-meta-headline">
										<?php the_field('concepts_branding_title', get_queried_object()); ?>
									</span>
									<p><?php the_field('concepts_branding_entrance', get_queried_object()); ?></p>
								</div>

							</div>

						</div>

						<div class="concept-main">

							<div class="concept-main-inner">

								<div class="concept-inner-block">
									<h4>Muziek</h4>
									<p><?php the_field('concepts_music', get_queried_object() ); ?></p>
								</div>

								<div class="concept-inner-block">
									<h4>Inkomprijzen</h4>
									<p><?php the_field('concepts_entrance', get_queried_object() ); ?></p>
								</div>

								<div class="concept-inner-block">
									<h4>Lidkaart</h4>
									<p><?php the_field('concepts_members', get_queried_object() ); ?></p>
								</div>

								<div class="concept-inner-block">
									<h4>Birthday</h4>
									<p><?php the_field('concepts_birthdays', get_queried_object() ); ?></p>
								</div>

								<div class="concept-inner-block">
									<h4>Dresscode</h4>
									<p><?php the_field('concepts_dresscode', get_queried_object() ); ?></p>
								</div>

								<div class="concept-inner-block">
									<h4>Minimum leeftijd</h4>
									<p><?php the_field('concepts_age', get_queried_object() ); ?></p>
								</div>
							</div>
							<?php if(get_field( 'concepts_link', get_queried_object() )){ ?>
							<a href="<?php the_field('concepts_link', get_queried_object() ); ?>" class="btn btn-wide" style="margin-bottom:20px;">Bezoek de website!</a>
							<? } ?>
							<?php if( get_queried_object()->term_id == 6 ) : ?>
							<a href="<?php echo get_permalink(450); ?>" class="btn btn-wide" target="_blank">Registreer als groep</a>
							<? else : ?>
							<a href="<?php echo get_permalink(15); ?>" class="btn btn-wide" target="_blank">Reserveer online!</a>
							<? endif; ?>
						</div>


						<?php /*
					endwhile;
				endif; */ ?>

			</div>

			<div class="half_column">

				<div class="main-events">

					<?php
					$now = (date('U') - 86400) * 1000;
					$coming_events_args = array(
						'post_type' => 'event',
						'posts_per_page' => 3,
			            'meta_key' => 'event_date',
			            'meta_query' => array(
							array(
								'key'     => 'event_date',
								'value'   => $now,
								'compare' => '>='
							)
						),
			            'orderby' => 'meta_value',
			            'order' => 'ASC'
					);

					$coming_events = new WP_Query( $coming_events_args );

					if( $coming_events->have_posts() ) :
						while( $coming_events->have_posts() ) :
							$coming_events->the_post();

							get_template_part('content', 'event');

						endwhile;
					endif;

					wp_reset_query();

					?>

				</div>

				<div class="concept-nav-small">
					
					<?php 

					$event_cat_args = array(
					    'orderby'           => 'menu_order', 
					    'order'             => 'ASC',
					    'hide_empty'        => false, 
					    'exclude'           => array( get_queried_object()->term_id ), 
					    'exclude_tree'      => array(), 
					    'include'           => array(),
					    'number'            => '', 
					    'fields'            => 'all', 
					    'slug'              => '', 
					    'parent'            => '',
					    'hierarchical'      => true, 
					    'child_of'          => 0, 
					    'get'               => '', 
					    'name__like'        => '',
					    'description__like' => '',
					    'pad_counts'        => false, 
					    'offset'            => '', 
					    'search'            => '', 
					    'cache_domain'      => 'core'
					); 

					$event_cats = get_terms('event_concepts', $event_cat_args);

					if( $event_cats ) :
						?><ul class="main-concept-list"><?php

						foreach( $event_cats as $cat ) :
							
							$term_link = get_term_link( $cat );

							?><li><?php

							if( $cat->slug == 'i-love-feestpaleis' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept ilfp">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_ilfp.png" alt="i<3FeestPaleis" />
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							if( $cat->slug == 'lovely-sundays' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept ls">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_ls.png" alt="Lovely Sundays" />
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							if( $cat->slug == 'after-work' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept aw">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_aw.png" alt="After Work" />
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							if( $cat->slug == 'level-3' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept l3">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_l3.png" alt="Lever 3" />
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							?></li><?php

						endforeach;

						?></ul><?php
					endif;

					?>

				</div>

				<div class="main-social">
			
					<?php get_template_part('content', 'social_list'); ?>

				</div>

			</div>

		</div>

	</div>
</div>

<?php get_footer(); ?>