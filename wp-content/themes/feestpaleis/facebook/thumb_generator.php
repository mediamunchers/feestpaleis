<?php

//
// Facebook Thumb Generator
//

function getFacebookThumb($src_embed, $dst_img, $dst_path, $dst_w, $dst_h, $dst_quality){
	//Get facebook image
	$src_cpl = $src_embed;
	//Define destination image
	$dst_cpl = $dst_path . basename($dst_img);
	//Get facebook image size
	list($src_w, $src_h) = getimagesize($src_cpl);
	//Create image from the thumbnail
	$src_img = imagecreatefromjpeg($src_cpl);
	//Calculate crop & scale factor
	if (($src_w > $dst_w) || ($src_h > $dst_h)){
		$x=$dst_w;
		$y=$dst_h;
		$idx=0;
		$idy=0;
		$idh=$dst_h;
		$idw=$dst_w;
		if (($src_w / $src_h) > ($dst_w / $dst_h)) { //in de breedte croppen
			$isy=0;
			$ish=$src_h;
			$isw=round(($src_w / ($src_w / $src_h)) * ($dst_w / $dst_h));
			$isx=round(($src_w - $isw)/2);
		} else { //in de hoogte croppen
			$isx=0;
			$isw=$src_w;
			$ish=round(($src_h / ($dst_w / $dst_h)) * ($src_w / $src_h));
			$isy=round(($src_h - $ish)/2);
		}
	}
	// Creating the resized image.
	$dst_img = imagecreatetruecolor($x,$y);
	imagecopyresampled($dst_img, $src_img, $idx, $idy, $isx, $isy, $idw, $idh, $isw, $ish);
	// Saving the resized image.
	imagejpeg($dst_img,$dst_cpl,$dst_quality);
	// Cleaning the memory.
	imagedestroy($src_img);
	imagedestroy($dst_img);
}

//
// Slug Generator
//

function generateSlug($str, $replace=array(), $delimiter='-', $lenghth='235') { //Input String, Speciale chars die moeten vervangen worden door spatie, char dat de spatie moet vervangen
	if( !empty($replace) ) {
		$str = str_replace((array)$replace, ' ', $str);
	}
	$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
	$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
	$clean = strtolower(trim($clean, '-'));
	$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
	if(strlen($clean) >= $lenghth){
		$clean = substr($clean, 0, $lenghth);
	} 
	return $clean;
}

?>

<?php
		require 'facebook.php';
		
		// Create our Application instance (replace this with your appId and secret).
		$facebook = new Facebook(array(
		  'appId'  => '194034240685000',
		  'secret' => '952a6cc2837aebf36cf1bf0200bd8000',
		));
		
		//the source url
		$url = mysql_real_escape_string($_POST['txtlink']);
		
		//get the facebook id's
		preg_match('/[\\?\\&]set=a.([^\\?\\&]+)/', $url, $match);
		
		//split them into an array
		$fbids = explode(".",$match[1]);
		
		//create the aid value for the fql query
		$fqlaid = $fbids[2]."_".$fbids[1];
		
		//create the fql photo query
		$fqlphotoquery = "SELECT src_big FROM photo WHERE aid=\"$fqlaid\" LIMIT 4";
		
		//Call the fql query - only works with public data, in our case photo's on fan pages
		$fbpictures = $facebook->api( array('method' => 'fql.query','query' => $fqlphotoquery,));
			
		//create the fql album info query
		$fqlalbuminfoquery = "SELECT name, location, created, modified, description, photo_count, link FROM album WHERE aid=\"$fqlaid\"";
		
		//Call the fql query - only works with public data, in our case album info on fan pages
		$fbalbuminfo = $facebook->api( array('method' => 'fql.query','query' => $fqlalbuminfoquery,));	
					
		$dst_img = $fbalbuminfo[0]['name']; // This name will be given to the resized image. - Album name (slug) is now used + a unique randrom string
		$dst_img_slug = generateSlug($dst_img); // Slug created from $dst_img
		$dst_path = '../assets/photos/'; // In this path the resized image will be saved
		$dst_w= '130'; // The width of the resized image
		$dst_h = '99'; // The height of the resized image
		$dst_quality = '80'; // Quality of the resized image (best quality = 100)
		
		$thumbstmp = array();
		
		for($i = 0; $i < 4; $i++){
			$src_embed = $fbpictures[$i]['src_big']; //Facebook image url
			$randomstr = random_string();
			$dst_img = $dst_img_slug . "-(" . $randomstr . ").jpg";
			getFacebookThumb($src_embed, $dst_img, $dst_path, $dst_w, $dst_h, $dst_quality);
			$thumbstmp[] = $dst_img;
		}
		

?>