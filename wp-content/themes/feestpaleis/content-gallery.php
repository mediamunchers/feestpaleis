<?php
$gallery_date = DateTime::createFromFormat('Ymd', get_field('gallery_date'));
$gallery_fburl = get_field('gallery_fblink');
$gallery_img_name = get_field('gallery_image');

$upload_dir = wp_upload_dir();
$gallery_img = $upload_dir['baseurl'] . '/fb/' . $gallery_img_name;



    $gallery_link = get_post_meta($post_id, 'gallery_fblink', true);

    //get the facebook id's
    preg_match('/[\\?\\&]set=a.([^\\?\\&]+)/', $gallery_fburl, $match);
    
    //split them into an array
    $fbids = explode(".",$match[1]);
    
    //create the aid value for the fql query
    $fqlaid = $fbids[2]."_".$fbids[1];
?>

<li>

	<a href="<?php echo $gallery_fburl; ?>" target="_blank">
		<img src="https://graph.facebook.com/<?php echo $fbids[0]; ?>/picture?type=album" class="no-resp" style="width:150%; max-width:150%;"/>
		<div class="media-mask">
			<div class="media-mask-inner">
				<div class="day"><?php echo $gallery_date->format('d'); ?></div>
				<div class="month"><?php echo $gallery_date->format('M'); ?></div>
				<div class="year"><?php echo $gallery_date->format('Y'); ?></div>
			</div>
		</div>
	</a>

</li>