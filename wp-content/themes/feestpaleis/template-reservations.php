<?php
/*
Template name: Reservations
*/
?>

<?php get_header(); ?>

<div class="main">
	<div class="container container-padding">

		<div class="half_column_last reservations-content">

			<div class="reservations-thumb">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/reservation_vip_tafel.jpg" alt="" />
				<ul>
					<li><a href="<?php echo get_permalink(15); ?>" class="active">VIP Tafel</a></li>
					<li><a href="<?php echo get_permalink(391); ?>">Limo service</a></li>
					<li><a href="<?php echo get_permalink(450); ?>">Groepen</a></li>
				</ul>
			</div>

			<div class="reservations-container">

				<div class="reservations-container-inner">

					<h5 class="color-main">Vip tafel</h5>
				    <?php
				    while ( have_posts() ) : the_post(); ?> 
			            <?php the_content(); ?> 
				    <?php
				    endwhile; 
				    wp_reset_query(); 
				    ?>
					<div class="reservations-options">
						<form id="res1_target" action="<?php echo get_permalink(142); ?>" method="post" class="res-stp1">
							
							<ul class="reservations1-options-list">
								<li><input class="res1_radiobtn" type="radio" name="res1_persons" value="4">VIP tafel vanaf 4 personen<span class="price">Vanaf &euro;120,00</span></a></li>
								<li><input class="res1_radiobtn" type="radio" name="res1_persons" value="8">VIP tafel vanaf 8 personen<span class="price">Vanaf &euro;240,00</span></a></li>
								<li><input class="res1_radiobtn" type="radio" name="res1_persons" value="12">VIP tafel vanaf 12 personen<span class="price">Vanaf &euro;360,00</span></a></li>
								<li><input class="res1_radiobtn" type="radio" name="res1_persons" value="16">VIP tafel vanaf 16 personen<span class="price">Vanaf &euro;480,00</span></a></li>
							</ul>

							<h5 class="color-main">Wenst u een ander aantal personen?</h5>
							
							<div class="blocky">
								<select id="res1_number_persons" name="res1_persons_select" required>
									<option value="0">Selecteer het gewenste aantal personen</option>
									<option value="5">VIP tafel 5 personen</option>
									<option value="6">VIP tafel 6 personen</option>
									<option value="7">VIP tafel 7 personen</option>
									<option value="9">VIP tafel 9 personen</option>
									<option value="10">VIP vanaf 10 personen</option>
									<option value="11">VIP vanaf 11 personen</option>
									<option value="13">VIP vanaf 13 personen</option>
									<option value="14">VIP vanaf 14 personen</option>
									<option value="15">VIP vanaf 15 personen</option>
									<option value="17">VIP vanaf 17 personen</option>
									<option value="18">VIP vanaf 18 personen</option>
									<option value="19">VIP vanaf 19 personen</option>
									<option value="20">VIP vanaf 20 personen</option>
								</select>
							</div>

							<input type="button" class="btn btn-wide btn-submit" value="Doorgaan" />

						</form>
					</div>

				</div>

			</div>

		</div>

		<div class="half_column">

			<div class="main-events blocky">

				<?php
					$now = (date('U') - 86400) * 1000;
				$coming_events_args = array(
					'post_type' => 'event',
					'posts_per_page' => 3,
			            'meta_key' => 'event_date',
			            'meta_query' => array(
							array(
								'key'     => 'event_date',
								'value'   => $now,
								'compare' => '>='
							)
						),
			            'orderby' => 'meta_value',
			            'order' => 'ASC'
				);

				$coming_events = new WP_Query( $coming_events_args );

				if( $coming_events->have_posts() ) :
					while( $coming_events->have_posts() ) :
						$coming_events->the_post();
					
						get_template_part('content', 'event');

					endwhile;
				endif;
				?>

			</div>

			<div class="concept-nav-small">
					
				<?php 

				$event_cat_args = array(
				    'orderby'           => 'menu_order', 
				    'order'             => 'ASC',
				    'hide_empty'        => false, 
				    'exclude'           => array(), 
				    'exclude_tree'      => array(), 
				    'include'           => array(),
				    'number'            => '', 
				    'fields'            => 'all', 
				    'slug'              => '', 
				    'parent'            => '',
				    'hierarchical'      => true, 
				    'child_of'          => 0, 
				    'get'               => '', 
				    'name__like'        => '',
				    'description__like' => '',
				    'pad_counts'        => false, 
				    'offset'            => '', 
				    'search'            => '', 
				    'cache_domain'      => 'core'
				); 

				$event_cats = get_terms('event_concepts', $event_cat_args);

				if( $event_cats ) :
					?><ul class="main-concept-list-big"><?php

					foreach( $event_cats as $cat ) :

						$term_link = get_term_link( $cat );

						?><li><?php

							if( $cat->slug == 'i-love-feestpaleis' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept ilfp">
										<div class="event-concept-inner">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_ilfp.png" alt="i<3FeestPaleis" />
										</div>
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							if( $cat->slug == 'lovely-sundays' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept ls">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_ls.png" alt="Lovely Sundays" />
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							if( $cat->slug == 'after-work' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept aw">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_aw.png" alt="After Work" />
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							if( $cat->slug == 'level-3' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept l3">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_l3.png" alt="Lever 3" />
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							?></li><?php

					endforeach;

					?></ul><?php
				endif;

				?>

			</div>

			<div class="main-social">
			
				<?php get_template_part('content', 'social_list'); ?>

			</div>

		</div>

	</div>
</div>

<?php get_footer(); ?>