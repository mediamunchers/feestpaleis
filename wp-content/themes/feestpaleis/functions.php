<?php

date_default_timezone_set('Europe/Brussels');

/*-----------------------------------------------------------------------------------*/
/* Remove Header Links */
/*-----------------------------------------------------------------------------------*/
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rel_canonical' ); // Display the canonical link rel
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );


/*-----------------------------------------------------------------------------------*/
/* Theme Settings */
/*-----------------------------------------------------------------------------------*/
// Disable admin-bar
add_filter('show_admin_bar', '__return_false');

// Post Thumbnail Init
add_theme_support( 'post-thumbnails' );


/*-----------------------------------------------------------------------------------*/
/* Project Enqueues */
/*-----------------------------------------------------------------------------------*/

// JAVASCRIPT
add_action( 'wp_enqueue_scripts', 'custom_enqueue_scripts' ); 
function custom_enqueue_scripts() {

  // jQuery
  wp_deregister_script('jquery');
  wp_deregister_script('jquery-ui');
  wp_register_script( 'jquery', 'http://code.jquery.com/jquery-latest.min.js');
  wp_register_script( 'jquery-ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js');
  wp_enqueue_script( 'jquery' );
  wp_enqueue_script( 'jquery-ui' );

  // Instafeed
  wp_enqueue_script( 'instafeed-js', get_template_directory_uri() . '/assets/js/instafeed.min.js', array(), '', true );
  
  // Tweetie
  wp_enqueue_script( 'tweetie-js', get_template_directory_uri() . '/assets/js/tweetie/tweetie.min.js', array(), '', true );
  
  // Sidex JS
  wp_enqueue_script( 'sidex-js', get_template_directory_uri() . '/assets/js/slidx.js', array(), '', true );

  // Mousewheel - fancybox dep
  wp_enqueue_script( 'mousewheel-js', get_template_directory_uri() . '/assets/js/jquery.mousewheel-3.0.6.pack.js', array(), '', true );


  // Fancybox
  wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/assets/js/jquery.fancybox.js', array(), '', true );
  wp_enqueue_script( 'fancybox-helper-buttons-js', get_template_directory_uri() . '/assets/js/helpers/jquery.fancybox-buttons.js', array(), '', true );
  wp_enqueue_script( 'fancybox-media-buttons-js', get_template_directory_uri() . '/assets/js/helpers/jquery.fancybox-media.js', array(), '', true );
  wp_enqueue_script( 'fancybox-thumbs-buttons-js', get_template_directory_uri() . '/assets/js/helpers/jquery.fancybox-thumbs.js', array(), '', true );

  // Custom JS
  wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/assets/js/custom.min.js', array( 'jquery', 'instafeed-js' ), '', true );

  // jQuery Validate
  if( is_page( array(15, 142, 146) ) ){
    wp_enqueue_script( 'placeholder-js', get_template_directory_uri() . '/assets/js/placeholders.min.js', array( 'jquery' ), '', true );  }

  // ddslick JS
  if( is_page(array(15, 142)) ) {
    wp_enqueue_script( 'ddslick-js', get_template_directory_uri() . '/assets/js/jquery.ddslick.min.js', array(), '', true );
  }

  if( is_page(15) ) {
    wp_enqueue_script('resStpOne-ui', get_template_directory_uri() . '/assets/js/reservations1.min.js', array(), '', true );
  }

  if( is_page(142) ) {
    wp_enqueue_script('jquery-ui');
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_script('resStpTwo-ui', get_template_directory_uri() . '/assets/js/reservations2.min.js', array(), '', true );
    wp_enqueue_script( 'timepicker-js', get_template_directory_uri() . '/assets/js/jquery-ui-timepicker-addon.js', array(), '', true );
  }

  if( is_singular('event') ) {
    wp_enqueue_script( 'rmf-tooltip-js', get_template_directory_uri() . '/assets/js/rmf-tooltip.min.js', array(), '', true );
  }

}

// CSS
add_action( 'wp_enqueue_scripts', 'custom_enqueue_styles' ); 
function custom_enqueue_styles() {

  // Google Fonts
  wp_enqueue_style( 'google-open-sans', 'http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800');
  wp_enqueue_style( 'google-open-sans-condensed', 'http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700');

  // Fancybox CSS
  wp_enqueue_style( 'fancybox', get_template_directory_uri() . '/assets/css/jquery.fancybox.css' );
  wp_enqueue_style( 'fancybox-buttons', get_template_directory_uri() . '/assets/js/helpers/jquery.fancybox-buttons.css' );
  wp_enqueue_style( 'fancybox-thumbs', get_template_directory_uri() . '/assets/js/helpers/jquery.fancybox-thumbs.css' );

  // Timepicker
  wp_enqueue_style( 'timepicker', get_template_directory_uri() . '/assets/css/timepicker.css' );

  // Jquery UI CSS
  if( is_page(142) ) {
    wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
  }

  // Custom CSS
  wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css' );

}


/*-----------------------------------------------------------------------------------*/
/* Project Enqueues // Backend */
/*-----------------------------------------------------------------------------------*/

// JAVASCRIPT
add_action( 'admin_enqueue_scripts', 'custom_admin_enqueue_scripts' ); 
function custom_admin_enqueue_scripts($page_hook) {

  global $post;

    if (!in_array($page_hook, array('post.php', 'post-new.php'))) {
        return;
    }
    if ($post->post_type!=='product') {
        return;
    }

    wp_enqueue_script ( 'custom-admin-js', get_stylesheet_directory_uri(). '/assets/js/custom-admin.min.js', array('jquery'), false, true );

}

// CSS
add_action( 'admin_enqueue_scripts', 'custom_admin_enqueue_styles' ); 
function custom_admin_enqueue_styles() {

  wp_enqueue_style ( 'admin-styles', get_stylesheet_directory_uri(). '/assets/css/admin-style.css' );

}




/*-----------------------------------------------------------------------------------*/
/* Menus init */
/*-----------------------------------------------------------------------------------*/
add_action( 'init', 'register_menus' );

function register_menus() {
  register_nav_menus(
    array(
      'sitenav' => __( 'Site Navigation', 'custom_theme_replace_me' ),
      'footnav' => __( 'Footer Navigation', 'custom_theme_replace_me' )
    )
  );
}


/*-----------------------------------------------------------------------------------*/
/* Custom current page selectors */
/*-----------------------------------------------------------------------------------*/

function remove_parent_classes($class)
{
  // check for current page classes, return false if they exist.
  return ($class == 'current_page_item' || $class == 'current_page_parent' || $class == 'current_page_ancestor'  || $class == 'current-menu-item') ? FALSE : TRUE;
}

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 3);
function special_nav_class($classes, $item){

     // if( is_singular( 'recipe') OR is_tax( 'cuisine' ) ) { //you can change the conditional from is_single() and $item->title
        
     //  $classes = array_filter($classes, "remove_parent_classes");

     //    if( $item->object_id == 1940 ){
     //      $classes[] = 'recipe-active';
     //    }

     // }
//var_dump($item);
    if( is_front_page() ) {
        
        $classes = array_filter($classes, "remove_parent_classes");

        if( $item->post_title == 'Concepts' ){
            $classes[] = 'current-menu-item';
        }

    }
    
    if( is_tax('event_concepts') ) {
        
        $classes = array_filter($classes, "remove_parent_classes");

        if( $item->post_title == 'Concepts' ){
            $classes[] = 'current-menu-item';
        }

    }


    if ( is_singular('event') ) {

        $classes = array_filter($classes, "remove_parent_classes");

        if( $item->object_id == 9 ){
            $classes[] = 'current-menu-item';
        }
    }


    if ( is_singular('gallery') ) {

        $classes = array_filter($classes, "remove_parent_classes");

        if( $item->object_id == 11 ){
            $classes[] = 'current-menu-item';
        }
    }

    if ( is_page(142) ) {

        $classes = array_filter($classes, "remove_parent_classes");

        if( $item->object_id == 15 ){
            $classes[] = 'current-menu-item';
        }
    }

    if ( is_page(146) ) {

        $classes = array_filter($classes, "remove_parent_classes");

        if( $item->object_id == 15 ){
            $classes[] = 'current-menu-item';
        }
    }

    if ( is_page(391) ) {

        $classes = array_filter($classes, "remove_parent_classes");

        if( $item->object_id == 15 ){
            $classes[] = 'current-menu-item';
        }
    }

    if ( is_page(394) ) {

        $classes = array_filter($classes, "remove_parent_classes");

        if( $item->object_id == 15 ){
            $classes[] = 'current-menu-item';
        }
    }

    if ( is_page(450) ) {

        $classes = array_filter($classes, "remove_parent_classes");

        if( $item->object_id == 15 ){
            $classes[] = 'current-menu-item';
        }
    }

    return $classes;
}



/*-----------------------------------------------------------------------------------*/
/* Selectively Hide WP Admin Menus */
/*-----------------------------------------------------------------------------------*/
add_action('admin_menu', 'ns_remove_admin_menus');
function ns_remove_admin_menus() {

  global $current_user;

  if ($current_user->ID != 1) {

    // Remove menu pages
    remove_menu_page('edit.php');
    remove_menu_page('edit-comments.php');

    // Remove WP Update nag
    add_action('admin_menu','wphidenag');
    function wphidenag() {
      remove_action( 'admin_notices', 'update_nag', 3 );
    }

    // Remove Plugin Update Nag
    remove_action( 'load-update-core.php', 'wp_update_plugins' );
    add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );

  } // endif;

}


/*-----------------------------------------------------------------------------------*/
/* Image Sizes */
/*-----------------------------------------------------------------------------------*/
//add_image_size( 'event_img', 560, 400, true );
//update_option('thumbnail_size_w', 280);
//update_option('thumbnail_size_h', 168);
//update_option('medium_size_w', 500);
//update_option('medium_size_h', 300);
//update_option('large_size_w', 680);
//update_option('large_size_h', 410);



/*-----------------------------------------------------------------------------------*/
/* Google Maps JS */
/*-----------------------------------------------------------------------------------*/
// function google_maps_code() {
//   if( is_page(17) ){
//     echo'
//     <script type="text/javascript"
//       src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC3WvbGhGu6pkYWVwBYIAt7J2hn-Hbowjo&sensor=false">
//     </script>
//     <script type="text/javascript">

//       function enableScrollingWithMouseWheel() {
//              map.setOptions({ scrollwheel: true });
//         }         
//         function disableScrollingWithMouseWheel() {
//              map.setOptions({ scrollwheel: false });
//         }

//         function initialize() {
//             var myLatlng = new google.maps.LatLng(50.946599, 4.429731)

//             var mapOptions = {
//                  center: myLatlng,
//                  zoom: 14,
//                  disableDefaultUI: true,
//                  zoomControl: true,
//                  zoomControlOptions: {
//                     position: google.maps.ControlPosition.LEFT_CENTER
//                  },
//                  scrollwheel:false
//             };
//             var map = new google.maps.Map(document.getElementById("map-canvas"),
//                    mapOptions);

//             // To add the marker to the map, use the "map" property
//             var marker = new google.maps.Marker({
//                    position: myLatlng,
//                    map: map,
//                    title:"Toyota Test Days"
//             });
//          google.maps.event.addListener(map, "onmousedown", function(){ enableScrollingWithMouseWheel(); });

//         }
//         google.maps.event.addDomListener(window, "load", initialize);

//         $("body").on("mousedown", function(event) {
//             var clickedInsideMap = $(event.target).parents("#map-canvas").length > 0;

//             if(!clickedInsideMap) {
//                 disableScrollingWithMouseWheel();
//             }
//         });

//         $(window).scroll(function() {
//             disableScrollingWithMouseWheel();
//         });



//     </script>

//     ';
//   }
// }
// add_action('wp_head', 'google_maps_code');

function google_maps_code() {
  if( is_page(17) ){
    echo'
    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC3WvbGhGu6pkYWVwBYIAt7J2hn-Hbowjo&sensor=false">
    </script>
    <script type="text/javascript">

      function initialize() {
        var myLatlng = new google.maps.LatLng(51.080368, 3.882227)
        var mapOptions = {
          center: myLatlng,
          zoom: 10
        };
        var map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);

            var marker = new google.maps.Marker({
                   position: myLatlng,
                   map: map,
                   title:"Danstheater Feestpaleis"
            });
      }
      google.maps.event.addDomListener(window, "load", initialize);

    </script>

    ';
  }
}
add_action('wp_head', 'google_maps_code');



/* #############################################################################################
 FACEBOOK
############################################################################################# */

//
// Facebook thumb
//

function getFacebookThumb($src_embed, $dst_img, $dst_path, $dst_w, $dst_h, $dst_quality){
  //Get facebook image
  $src_cpl = $src_embed;
  //Define destination image
  $dst_cpl = $dst_path . basename($dst_img);
  //Get facebook image size
  list($src_w, $src_h) = getimagesize($src_cpl);
  //Create image from the thumbnail
  $src_img = imagecreatefromjpeg($src_cpl);
  //Calculate crop & scale factor
  if (($src_w > $dst_w) || ($src_h > $dst_h)){
    $x=$dst_w;
    $y=$dst_h;
    $idx=0;
    $idy=0;
    $idh=$dst_h;
    $idw=$dst_w;
    if (($src_w / $src_h) > ($dst_w / $dst_h)) { //in de breedte croppen
      $isy=0;
      $ish=$src_h;
      $isw=round(($src_w / ($src_w / $src_h)) * ($dst_w / $dst_h));
      $isx=round(($src_w - $isw)/2);
    } else { //in de hoogte croppen
      $isx=0;
      $isw=$src_w;
      $ish=round(($src_h / ($dst_w / $dst_h)) * ($src_w / $src_h));
      $isy=round(($src_h - $ish)/2);
    }
  }
  // Creating the resized image.
  $dst_img = imagecreatetruecolor($x,$y);
  imagecopyresampled($dst_img, $src_img, $idx, $idy, $isx, $isy, $idw, $idh, $isw, $ish);
  // Saving the resized image.
  imagejpeg($dst_img,$dst_cpl,$dst_quality);
  // Cleaning the memory.
  imagedestroy($src_img);
  imagedestroy($dst_img);
}

//
// Slug Generator
//

function generateSlug($str, $replace=array(), $delimiter='-', $lenghth='235') { //Input String, Speciale chars die moeten vervangen worden door spatie, char dat de spatie moet vervangen
  if( !empty($replace) ) {
    $str = str_replace((array)$replace, ' ', $str);
  }
  $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
  $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
  $clean = strtolower(trim($clean, '-'));
  $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
  if(strlen($clean) >= $lenghth){
    $clean = substr($clean, 0, $lenghth);
  } 
  return $clean;
}

//
// Random String Generator
//

function random_string(){
  $character_set_array = array( );
  $character_set_array[ ] = array( 'count' => 5, 'characters' => 'abcdefghijklmnopqrstuvwxyz' );
  $character_set_array[ ] = array( 'count' => 2, 'characters' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' );
  $character_set_array[ ] = array( 'count' => 2, 'characters' => '0123456789' );
  $character_set_array[ ] = array( 'count' => 1, 'characters' => '-_' );
  $temp_array = array( );
  foreach ( $character_set_array as $character_set )
  {
    for ( $i = 0; $i < $character_set[ 'count' ]; $i++ )
    {
    $temp_array[ ] = $character_set[ 'characters' ][ rand( 0, strlen( $character_set[ 'characters' ] ) - 1 ) ];
    }
  }
  shuffle( $temp_array );
  return implode( '', $temp_array );
}

//
// Facebook gedeelte
//

require_once ( 'includes/facebook.php' );

//add_action('acf/save_post', 'my_acf_save_post', 1); // Doet actie voor functie my_acf_save_post
add_action('acf/save_post', 'my_acf_save_post', 20); //doet actie na functie my_acf_save_post

function my_acf_save_post($post_id) {

  $post_type = get_post_type($post_id);

  if($post_type == 'gallery'){

    $config = array(
      'appId'  => '194034240685024',
          'secret' => '952a6cc2837aebf36cf1bf0200bd8087'
    );

    // Create our Application instance (replace this with your appId and secret).
    $facebook = new Facebook($config);

    $gallery_link = get_post_meta($post_id, 'gallery_fblink', true);

    //get the facebook id's
    preg_match('/[\\?\\&]set=a.([^\\?\\&]+)/', $gallery_link, $match);
    
    //split them into an array
    $fbids = explode(".",$match[1]);
    
    //create the aid value for the fql query
    $fqlaid = $fbids[2]."_".$fbids[1];
    
    //create the fql photo query
    $fqlphotoquery = "SELECT src_big FROM photo WHERE aid=\"$fqlaid\" LIMIT 4";
    
    //Call the fql query - only works with public data, in our case photo's on fan pages
    $fbpictures = $facebook->api( array('method' => 'fql.query','query' => $fqlphotoquery,));
      
    //create the fql album info query
    $fqlalbuminfoquery = "SELECT name, location, created, modified, description, photo_count, link FROM album WHERE aid=\"$fqlaid\"";
    
    //Call the fql query - only works with public data, in our case album info on fan pages
    $fbalbuminfo = $facebook->api( array('method' => 'fql.query','query' => $fqlalbuminfoquery,));  
      
    $dst_img = $fbalbuminfo[0]['name']; // This name will be given to the resized image. - Album name (slug) is now used + a unique randrom string
    $dst_img_slug = generateSlug($dst_img); // Slug created from $dst_img
    $dst_path = '../wp-content/uploads/fb/'; // In this path the resized image will be saved
    $dst_w= '300'; // The width of the resized image
    $dst_h = '300'; // The height of the resized image
    $dst_quality = '80'; // Quality of the resized image (best quality = 100)

    $img_url = $fbpictures[0]['src_big'];

    $post_title = $fbalbuminfo[0]['name'];
    
    for($i = 0; $i < 1; $i++){
      $src_embed = $fbpictures[$i]['src_big']; //Facebook image url
      $randomstr = random_string();
      $dst_img = $dst_img_slug . "-(" . $randomstr . ").jpg";
      getFacebookThumb($src_embed, $dst_img, $dst_path, $dst_w, $dst_h, $dst_quality);
      $thumbstmp[] = $dst_img;
    }


      // unhook this function so it doesn't loop infinitely
      remove_action('save_post', 'change_title');

      // update the post, which calls save_post again
      wp_update_post(array('ID' => $post_id, 'post_title' => $post_title));
    
    // add image to database
    update_post_meta($post_id, 'gallery_image', $thumbstmp[0]);

      // re-hook this function
      add_action('save_post', 'change_title');
  }
} 

//
// Set default titel bij toeveoegen galerij
//

if(isset($_GET['post_type'])){
  $custom_wp_post_type = $_GET['post_type'];
}else{
  $custom_wp_post_type = "none";
}

if($custom_wp_post_type == "gallery") add_filter( 'default_title', 'my_editor_title' );


function my_editor_title( $title ) {

  $title = "Facebook gallery";

  return $title;
}




//
// Create unix timestamp date to sort albums
//

// CREATE UNIX TIME STAMP FROM DATE PICKER
function custom_unixtimesamp ( $post_id ) {
    if ( get_post_type( $post_id ) == 'events' ) {
  $startdate = get_post_meta($post_id, 'startdate', true);

    if($startdate) {
      $dateparts = explode('/', $startdate);
      $newdate1 = strtotime(date('d.m.Y H:i:s', strtotime($dateparts[1].'/'.$dateparts[0].'/'.$dateparts[2])));
      update_post_meta($post_id, 'unixstartdate', $newdate1  );
    }
  }
}
add_action( 'save_post', 'custom_unixtimesamp', 100, 2);




/*-----------------------------------------------------------------------------------*/
/* Includes */
/*-----------------------------------------------------------------------------------*/
require_once ( 'includes/cpts.php' );
require_once ( 'includes/fp_reservation.php' );
//require_once ( 'includes/sidebars.php' );
//require_once ( 'includes/widgets.php' );
//require_once ( 'includes/shortcodes.php' );

?>
