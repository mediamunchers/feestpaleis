<?php
/*
Template name: Full width
*/
?>

<?php get_header(); ?>



<div class="main">
	<div class="container container-padding">

		<div class="row">

			<div class="one_full_column">

				<div class="one_column_inner">

				<?php
				if( have_posts() ) :
					while( have_posts() ) :
						the_post() ;

					get_template_part('content', 'page');

					endwhile;
				endif;
				?>

				</div>

			</div>

		</div>

	</div>
</div>

<?php get_footer(); ?>