<?php

get_header('slider'); ?>

<div class="grid content">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<div class="introduction">
				<?php the_content(); ?>
			</div>

			<div class="contact_home">
				<h3>Visit us</h3>
				<p>Zwarte Zusterstraat 11<br />
				9300 Aalst</p>

				<p>T: 053 62 28 70<br />
				E: <a href="mailto:info@detroitaalst.be">info@detroitaalst.be</a></p>
			</div>

		<?php endwhile; endif; ?>

</div>

<?php
	
	$order = new WC_Order( 405 );
	$items = $order->get_items();

?>

<div class="row dark_bg">

	<div class="grid products">

		<div class="title"><h2>Limited editions</h2></div>

		<ul class="products_grid4">
			<?php
			$args = array( 'post_type' => 'product', 'posts_per_page' => 8, 'product_cat' => 'featured' );
			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ) {
				while ( $loop->have_posts() ) : $loop->the_post();

					woocommerce_get_template_part( 'content', 'product' );

				endwhile;
			} else {
				echo __( 'No products found' );
			}
			wp_reset_postdata();
			?>
		</ul>

	</div>

	<div class="grid brands_home">
		<div class="title"><h3>Onze merken:</h3></div>
			<ul>
				

				<?php
				//list terms in a given taxonomy using wp_list_categories (also useful as a widget if using a PHP Code plugin)

				$args = array(
				  'taxonomy'     => 'product_cat',
				  'orderby'      => 'id', 
				  'show_count'   => 8,
				  'hierarchical' => 1,
				  'parent'		 => 19,
				  'title_li'     => '',
				  'hide_empty'	 => 0
				);

				$categories = get_terms( 'product_cat', $args);
				foreach($categories as $category) { ?>
					<li>
							<?php
							$term_link = get_term_link( $category, 'product_cat' );
						    echo '<a href="' . $term_link . '">';
						    ?>
						<img src="<?php bloginfo('stylesheet_directory'); ?>/images/brands/brands-<?php echo $category->name; ?>-logo.png">
							<?php
							echo '</a>';
							?>
					</li>
				<?php  } ?>


			</ul>
	</div>

	</div>
</div>

<div class="grid bottom" id="wysija">
	<div class="box vernissages">
		<h3>Terms</h3>
		<p>Read our <a href="<?php get_permalink(456); ?>">terms and conditions</a></p>
	</div>
	<div class="box shipping">
		<h3>Shipping</h3>
		<p>When an unknown printer took a galley of type and scrambled. <br />
		Lorem Ipsum has been the industry's standard dummy text.</p>
	</div>
	<div class="box membership">
		<h3>Newsletter</h3>
		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry an unknown printer took a galley.</p>
		<div class="subscribe">
			<?php mc4wp_form(); ?>
		</div>
	</div>
</div>

<div class="row mod-instagram">
	<div class="grid">
		<div class="title">
			<h3>Instagram</h3>
			<?php echo do_shortcode( '[simply_instagram endpoints="users" type="recent-media" size="standard_resolution" display="5"]' ); ?>
	</div>
</div>

<?php get_footer(); ?>