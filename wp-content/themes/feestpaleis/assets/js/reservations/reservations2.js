jQuery(document).ready(function($) {

	//ACCORDION BUTTON ACTION	
	$('div.drinks_menu_title').click(function() {
		$('div.drinks_menu_container').slideUp('normal');
		$(this).next().slideDown('normal');
	});
 
	//HIDE THE DIVS ON PAGE LOAD	
	$("div.drinks_menu_container:not(:first)").hide();


	// Add price inline with name product
	$('.drinks_menu_container > select option').each(function(){
		var option_price = $(this).attr('data-description');
		var option_name = $(this).text();
		
		if( $(this).val() !== 0 ){
			$(this).text(option_price + " - " +option_name);
		}
	});

	// TOTAL PRICE	
	window.int_persons = parseInt( $('.res2_persons_placeholder').val() );
	// OG CODE window.res_total = parseInt( 125 + ( (int_persons - 4) * 25) );
	window.res_total = parseInt( 30 * int_persons );


	// REST PRICE	
	var rest_price = res_total - ( int_persons * 10 );
	$(".res2_container_drinks_rest span.res_rest_val").html(rest_price);

	var init_new_price_sum = 0;
	$('.drinks_menu_container > select option:selected').each(function(){
		var init_selected_option_price = $(this).attr('data-description');
		var init_rest_price_new = init_selected_option_price.substring(2);
		init_rest_price_new = parseFloat(init_rest_price_new.replace(',','.').replace(' ',''));
		
		init_new_price_sum = init_new_price_sum + init_rest_price_new;
		window.init_new_price = (rest_price - init_new_price_sum).toFixed(2);
	});

	if( init_new_price > 0){

		$(".reservation-total span.res_tot_val").html(res_total);
		$(".res2_container_drinks_rest").html('Resterend: &euro;<span class="res_rest_val">' + init_new_price + '</span>' );

	} else {
		
		init_new_price = Math.abs(init_new_price);
		init_new_res_total = res_total + init_new_price;

		$(".res2_container_drinks_rest").html("&euro; " + init_new_price.toFixed(2) + " supplement");
		$(".reservation-total span.res_tot_val").html(init_new_res_total);

	}





	// Recalculate to spend price
	$('.drinks_menu_container, .res_container_extras').on( 'change', 'select', function(){

		res_total = parseInt( 30 * int_persons );

		var res_extra_name = $('#res_extras2 option:selected').val();
		if( res_extra_name === 'shuttle' ){
			res_total = res_total + 50;
		} else if( res_extra_name === 'limo' ){
			res_total = res_total + 295;
		} else if( res_extra_name === 'hotel' ){
			res_total = res_total + 240;
		} else {
			res_total = res_total + 0;
		}



		var new_price_sum = 0;
		$('.drinks_menu_container > select option:selected').each(function(){
			
			var selected_option_price = $(this).attr('data-description');
			var rest_price_new = selected_option_price.substring(2);
			rest_price_new = parseFloat(rest_price_new.replace(',','.').replace(' ',''));
			
			new_price_sum = new_price_sum + rest_price_new;
			window.new_price = (rest_price - new_price_sum).toFixed(2);

			
			if( new_price > 0){

				$(".res2_container_drinks_rest").html('Resterend: &euro;<span class="res_rest_val"></span>' );
				$(".reservation-total span.res_tot_val").html(res_total);

			} else {
				
				new_price = Math.abs(new_price)
				new_res_total = res_total + new_price;

				$('.res2_container_drinks_rest').html('&euro;<span class="res_rest_val"></span> supplement');
				$('.reservation-total span.res_tot_val').html(new_res_total);

			}

		});

		$(".res2_container_drinks_rest span.res_rest_val").html(new_price);
		
	});


	$('.drinks_menu_container').on( 'change', 'select:last-of-type', function(){

		// Add new field
		var prev_select = $(this).html();
		var prev_select_name = $(this).closest('.drinks_menu_container > select:last-of-type').attr('id');
		var next_select_id = parseInt( prev_select_name.substr(prev_select_name.length - 1)) + 1;
		var next_select_type = prev_select_name.substr(prev_select_name.length - 4);
		
		next_select_type = next_select_type.substr( 0, 3);
		
		if( next_select_type === 'bub' ){
			$(this).parent().append('<select name="res_val_bub' + next_select_id + '" id="res_drinks_bub' + next_select_id + '" class="ddslick-me">' + prev_select + '</select>');
		} else if( next_select_type === 'str' ){
			$(this).parent().append('<select name="res_val_str' + next_select_id + '" id="res_drinks_str' + next_select_id + '" class="ddslick-me">' + prev_select + '</select>');
		} else if( next_select_type === 'sof' ){
			$(this).parent().append('<select name="res_val_sof' + next_select_id + '" id="res_drinks_sof' + next_select_id + '" class="ddslick-me">' + prev_select + '</select>');
		}

	});

	$('.drinks_menu_container').on( 'change', 'select:nth-last-child(2)', function(){

		if( this.value === 0 ){
			$(this).parent('.drinks_menu_container').find('select.ddslick-me:last-of-type').remove();
		}

	});


	// window.res_extra = 0;
	// $('.res_container_block').on( 'change', 'select#res_extras2', function(){


	// 	if( $(this).val() == 'shuttle' ){
	// 		res_extra = 50;
	// 	} else if( $(this).val() == 'limo' ){
	// 		res_extra = 295;
	// 	} else if( $(this).val() == 'hotel' ){
	// 		res_extra = 240;
	// 	} else {
	// 		res_extra = 0;
	// 	}

	// 	var new_total_price = new_res_total + res_extra;

	// 	$(".reservation-total span.res_tot_val").html(new_total_price);

	// });

	$("#res_date").datetimepicker({
        minDate: 0,
        changeMonth: true,
        numberOfMonths: 1,
		theme: true,
		dateFormat : "dd M yy",
		timeFormat: "HH:mm",
		hourText: "Uur",
		minuteText: "Minuten",
		stepMinute: 15,
		minuteGrid: 15,
		controlType: "slider"
		// ,beforeShow: function() {
		// 	$("#ui-datepicker-div").wrap("<div class=\"ll-skin-melon\" />");
		// },
	});

	// Submit ghost for total price	
	$('.btn-submit').click(function() {
		var total = $('.res_tot_val').text();
		$('.res2_price_placeholder').val(total);

		if( $('.res2_container_drinks_rest').html().substr(0, 3) === 'Res' ){
			$('.res2_container_drinks_rest').fadeTo(100, 0.1).fadeTo(200, 1.0);
			$('.res_errors').hide().html("U dient het resterende bedrag volledig te gebruiken.").fadeIn('slow');
		} else if( $('#res_date').val() === '') {
			$('.res_errors').hide().html("U dient een datum in te vullen.").fadeIn('slow');
		} else { 
			$("#res2_target").submit();
		}

	});








		/* @@@@@@@@@@@@@@@@@@@@@@@@@@ TRASH @@@@@@@@@@@@@@@@@@@@@@@@@@ */


	// window.bubbles_price = 0;
	// window.strong_price = 0;
	// window.soft_price = 0;

	// $('#drinks_menu .drinks_menu_container > select.ddslick-me').each( function( index, value ){
	// 	$(this).ddslick({
	// 		width: "100%",
	// 		imagePosition: "left",
	// 		background: "#FFFFFF",
	// 		selectText: "Selecteer uw gewenste dranken",
	// 		onSelected: function(data){
	// 			if( data.selectedData.value == 'shuttle' ){
	// 				var res_total_new = 50;
	// 			} else if( data.selectedData.value == 'limo' ){
	// 				var res_total_new = 120;
	// 			} else if( data.selectedData.value == 'hotel' ){
	// 				var res_total_new = 240;
	// 			} else {
	// 				var res_total_new = 0;
	// 			}
	// 			var newer = res_total + res_total_new;
	// 			$(".reservation-total span.res_tot_val").html(newer);
	// 			$("#res_extras2 input.dd-selected-value").attr("name", "res2_extras");
	// 		}
	// 	});
	// });


		// $("#res_drinks_bubbels1").ddslick({
	// 	width: "100%",
	// 	imagePosition: "left",
	// 	background: "#FFFFFF",
	// 	onSelected: function(data){
	// 		// $("#res_drinks_bubbels1 input.dd-selected-value").attr("name", "res_val_bubbles1");
	// 		// var rest_price = res_total - (15 + ( int_persons * 10) );
	// 		// bubbles_price = data.selectedData.description;
	// 		// bubbles_price = bubbles_price.substring(2);
	// 		// bubbles_price = parseFloat(bubbles_price.replace(',','.').replace(' ',''));
	// 		// rest_price_new = (rest_price - bubbles_price - strong_price - soft_price).toFixed(2);
	// 		// if( rest_price_new < 0){
	// 		// 	$(".res2_container_drinks_rest").html("&euro; " + rest_price_new.toFixed(2) + " supplement");
	// 		// } else {
	// 		// 	$(".res2_container_drinks_rest span.res_rest_val").html(rest_price_new);
	// 		// }
	// 		$(".drinks_menu_container").children('.dd-container:last').addClass('fuckme');
	// 	}
	// });

	// $("#res_drinks_bubbels2").ddslick({
	// 	width: "100%",
	// 	imagePosition: "left",
	// 	background: "#FFFFFF",
	// 	onSelected: function(data){
	// 		$("#res_drinks_bubbels2 input.dd-selected-value").attr("name", "res_val_bubbles2");
	// 		var rest_price = res_total - (15 + ( int_persons * 10) );
	// 		bubbles_price = data.selectedData.description;
	// 		bubbles_price = bubbles_price.substring(2);
	// 		bubbles_price = parseFloat(bubbles_price.replace(',','.').replace(' ',''));
	// 		rest_price_new = (rest_price - bubbles_price - strong_price - soft_price).toFixed(2);
	// 		if( rest_price_new < 0){
	// 			$(".res2_container_drinks_rest").html("&euro; " + rest_price_new.toFixed(2) + " supplement");
	// 		} else {
	// 			$(".res2_container_drinks_rest span.res_rest_val").html(rest_price_new);
	// 		}

	// 	}
	// });





	// $("#res_drinks_strong1").ddslick({
	// 	width: "100%",
	// 	imagePosition: "left",
	// 	background: "#FFFFFF",
	// 	onSelected: function(data){
	// 		$("#res_drinks_strong1 input.dd-selected-value").attr("name", "res_val_strong1");
	// 		var rest_price = res_total - (15 + ( int_persons * 10) );
	// 		strong_price = data.selectedData.description;
	// 		strong_price = strong_price.substring(2);
	// 		strong_price = parseFloat(strong_price.replace(',','.').replace(' ',''));
	// 		rest_price_new = (rest_price - bubbles_price - strong_price - soft_price).toFixed(2);
	// 		if( rest_price_new < 0){
	// 			$(".res2_container_drinks_rest").html("&euro; " + rest_price_new.toFixed(2) + " supplement");
	// 		} else {
	// 			$(".res2_container_drinks_rest span.res_rest_val").html(rest_price_new);
	// 		}
	// 	}
	// });

	// $("#res_drinks_strong2").ddslick({
	// 	width: "100%",
	// 	imagePosition: "left",
	// 	background: "#FFFFFF",
	// 	onSelected: function(data){
	// 		$("#res_drinks_strong2 input.dd-selected-value").attr("name", "res_val_strong2");
	// 		var rest_price = res_total - (15 + ( int_persons * 10) );
	// 		strong_price = data.selectedData.description;
	// 		strong_price = strong_price.substring(2);
	// 		strong_price = parseFloat(strong_price.replace(',','.').replace(' ',''));
	// 		rest_price_new = (rest_price - bubbles_price - strong_price - soft_price).toFixed(2);
	// 		if( rest_price_new < 0){
	// 			$(".res2_container_drinks_rest").html("&euro; " + rest_price_new.toFixed(2) + " supplement");
	// 		} else {
	// 			$(".res2_container_drinks_rest span.res_rest_val").html(rest_price_new);
	// 		}
	// 	}
	// });




	// $("#res_drinks_soft1").ddslick({
	// 	width: "100%",
	// 	imagePosition: "left",
	// 	background: "#FFFFFF",
	// 	onSelected: function(data){
	// 		$("#res_drinks_soft1 input.dd-selected-value").attr("name", "res_val_soft1");
	// 		var rest_price = res_total - (15 + ( int_persons * 10) );
	// 		soft_price = data.selectedData.description;
	// 		soft_price = soft_price.substring(2);
	// 		soft_price = parseFloat(soft_price.replace(',','.').replace(' ',''));
	// 		rest_price_new = (rest_price - bubbles_price - strong_price - soft_price).toFixed(2);
	// 		if( rest_price_new < 0){
	// 			$(".res2_container_drinks_rest").html("&euro; " + rest_price_new.toFixed(2) + " supplement");
	// 		} else {
	// 			$(".res2_container_drinks_rest span.res_rest_val").html(rest_price_new);
	// 		}
	// 	}
	// });


	// $("#res_drinks_soft2").ddslick({
	// 	width: "100%",
	// 	imagePosition: "left",
	// 	background: "#FFFFFF",
	// 	onSelected: function(data){
	// 		$("#res_drinks_soft2 input.dd-selected-value").attr("name", "res_val_soft2");
	// 		var rest_price = res_total - (15 + ( int_persons * 10) );
	// 		soft_price = data.selectedData.description;
	// 		soft_price = soft_price.substring(2);
	// 		soft_price = parseFloat(soft_price.replace(',','.').replace(' ',''));
	// 		rest_price_new = (rest_price - bubbles_price - strong_price - soft_price).toFixed(2);
	// 		if( rest_price_new < 0){
	// 			$(".res2_container_drinks_rest").html("&euro; " + rest_price_new.toFixed(2) + " supplement");
	// 		} else {
	// 			$(".res2_container_drinks_rest span.res_rest_val").html(rest_price_new);
	// 		}
	// 	}
	// });

	// $("#res_extras2").ddslick({
	// 	width: "100%",
	// 	imagePosition: "left",
	// 	background: "#FFFFFF",
	// 	selectText: "Selecteer uw gewenste dranken",
	// 	onSelected: function(data){
	// 		if( data.selectedData.value == 'shuttle' ){
	// 			var res_total_new = 50;
	// 		} else if( data.selectedData.value == 'limo' ){
	// 			var res_total_new = 120;
	// 		} else if( data.selectedData.value == 'hotel' ){
	// 			var res_total_new = 240;
	// 		} else {
	// 			var res_total_new = 0;
	// 		}
	// 		var newer = res_total + res_total_new;
	// 		$(".reservation-total span.res_tot_val").html(newer);
	// 		$("#res_extras2 input.dd-selected-value").attr("name", "res2_extras");
	// 	}
	// });

	// $("#res_extras2").ddslick({
	// 	width: "100%",
	// 	imagePosition: "left",
	// 	background: "#FFFFFF",
	// 	selectText: "Selecteer uw gewenste dranken",
	// 	onSelected: function(data){
	// 		if( data.selectedData.value == 'shuttle' ){
	// 			var res_total_new = 50;
	// 		} else if( data.selectedData.value == 'limo' ){
	// 			var res_total_new = 120;
	// 		} else if( data.selectedData.value == 'hotel' ){
	// 			var res_total_new = 240;
	// 		} else {
	// 			var res_total_new = 0;
	// 		}
	// 		var newer = res_total + res_total_new;
	// 		$(".reservation-total span.res_tot_val").html(newer);
	// 		$("#res_extras2 input.dd-selected-value").attr("name", "res2_extras");
	// 	}
	// });


});