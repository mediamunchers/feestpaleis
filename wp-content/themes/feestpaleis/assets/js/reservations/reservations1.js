jQuery(function(){

    // Validation
    $('.btn-submit').click(function(){

        // $('#res1_target').submit(function(e){
        //     return false;
        // });

        if( $('.dd-selected-value').val() == 0 ) {
            $('#res1_number_persons').find('.dd-select').css('border', '#bf0101 solid 1px');
            $('#res1_number_persons').find('.dd-selected-text').css('color', '#bf0101', 'important');
        } else {
            $('#res1_target').submit();
        }

    });

    // ...

    $("#res1_target input.res1_radiobtn").removeAttr("checked");

    $( ".reservations1-options-list li" ).click( function(e) {
        $(this).children( ".res1_radiobtn" ).prop("checked",true);
        $("#res1_target").submit();
    });

    $("#res1_number_persons").ddslick({
        width: "100%",
        background: "#ffffff",
        selectText: "Selecteer het gewenste aantal personen",
        onSelected: function(data){
            $("#res1_number_persons input.dd-selected-value").attr("name", "res_val_drink1");
        }
    });

});