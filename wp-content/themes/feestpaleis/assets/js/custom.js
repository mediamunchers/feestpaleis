jQuery(function($){

	/* INSTAFEED */
	var feed = new Instafeed ({
		get: 'tagged',
		tagName: 'feestpaleis',
		clientId: 'ce7b1423423449a7bfb64b58da2c5c14',
		limit: '12',
		resolution: 'thumbnail',
		template: '<li><a href="{{link}}"><img src="{{image}}" /></a></li>',
		useHttp: true   // <--- add this
	});
	feed.run();

	/* TWEETIE */
	$('.tweets').twittie({
		'count': 2,
		'dateFormat': '%b/%d/%Y',
        'template': '<div class="twittericon"><div class="twittermsg">{{tweet}}</div></div>',
    	'apiPath': 'http://www.feestpaleis.be/wp-content/themes/feestpaleis/assets/js/tweetie/api/tweet.php'
    	//'apiPath': 'http://www.time2change.com/projects/feestpaleis/wp-content/themes/feestpaleis/assets/js/tweetie/api/tweet.php'
    	//'apiPath': 'http://localhost/feestpaleis/wp-content/themes/feestpaleis/assets/js/tweetie/api/tweet.php'
	});

	/* SOCIAL MEDIA HOVERS */
	$('.soc-hover')
	.find('span')
	.hide()
	.end()
	.hover(function() {
		$(this).find("span").stop(true, true).fadeIn(200);
	}, function() {
		$(this).find("span").stop(true, true).fadeOut(200);
	});

	/* CONCEPTS HOVERS */
	$('ul.main-concept-list-big li, ul.main-concept-list-small li, ul.main-concept-list li').hover(function() {
		$(this).find("span").stop(true, true).fadeIn(200);
	}, function() {
		$(this).find("span").stop(true, true).fadeOut(200);
	});


	/* MEDIA HOVERS */
	$('ul.grid-gallery-media li').hover(function() {
		$(this).find(".media-mask").stop(true, true).fadeIn(200).css('display','table');
	}, function() {
		$(this).find(".media-mask").stop(true, true).fadeOut(200);
	});


	/* Fancybox */

	// Gallery
	/*
	$('.fancybox').fancybox({
		padding: 10,
		helpers: {
			overlay: {
				locked: false,
				css: {
					'background' : 'rgba(255, 255, 255, 0.95)'
				}
			}
		}
	});
	*/
	
	$('.fancybox').fancybox({
		padding: 10,
		closeBtn : false,
		arrows: false,
		helpers: { 
			title: null,
			buttons: {},
			overlay : { css : { 'background' : 'rgba(255,255,255,0.9)' }, locked: false }
		},
	});
	
	

	// Contactform
	//$(".btn-contactform").fancybox();

	/* Reservations */

	/* STEP 2 */
	// $('.dd-selected').change( function(){

		// max_drinks_price = 125 + ( ( $(this).val() - 4) * 25 ) - ( 15 + ( $(this).val() * 10 ) );
		// window.alert(max_drinks_price);

		// if( $(this).val() == '4' ){
			
		// }else if( $(this).val() == '8' ){
			
		// }

	// })


	/* jQuery responsiveness */
	function changeRsvpLeftPos(){
		$('.main-event').each(function(){
	    	var dateWidth = $(this).find('.main-event-date').width();
	    	var newWidth = parseInt(dateWidth+31);
	    	$(this).find('a.rsvp').css('left', newWidth);
	    	//console.log(dateWidth);
	    });
	}
	changeRsvpLeftPos();
	$(window).resize(function() {
        changeRsvpLeftPos();
    });


    /* SOCIAL MEDIA HOVERS */
	$('ul.main-gallery-grid-full li').hover( function(){
		$(this).find('.main-gallery-overlay').stop(true).fadeIn(200);
	}, function() {
		$(this).find('.main-gallery-overlay').stop(true).fadeOut(200);
	});

	$('ul.side-gallery-grid-full li').hover( function(){
		$(this).find('.main-gallery-overlay').stop(true).fadeIn(200);
	}, function() {
		$(this).find('.main-gallery-overlay').stop(true).fadeOut(200);
	});


});