<?php
?><!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]><html class="ie ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if lt IE 9]> <html class="old-ie"> <![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<meta property="og:title" content="<?php wp_title( '|', true, 'right' ); ?><?php bloginfo('name'); ?>" />
<meta property="og:site_name" content="Danstheater Feestpaleis Beervelde"/>
<meta property="og:url" content="<? get_permalink(); ?>" />
<meta property="og:image" content="<?php bloginfo('stylesheet_directory');?>/images/theme/venue_smokelounge.jpg"/>
<meta property="og:description" content="<?php bloginfo('description'); ?> "/>
<meta property="fb:app_id" content="194034240685024" />
<title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo('name'); ?></title>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/nl_NL/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="header">
	<div class="container">
		<!--
		<div class="topnav">
			<div class="members-area">
				<a href="#" class="member-area">Members Area</a>
			</div>
		</div>
		-->
		<div class="logo">
			<a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('stylesheet_directory');?>/images/theme/logo_fp.jpg" alt="" /></a>
		</div>

		<div class="sitenav">

			<?php

			$sitenav_args = array(
				'theme_location'  => 'sitenav',
				'menu'            => '',
				'container'       => '',
				'container_class' => '',
				'container_id'    => '',
				'menu_class'      => 'sitenav',
				'menu_id'         => '',
				'echo'            => true,
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'depth'           => 0,
				'walker'          => ''
			);

			wp_nav_menu( $sitenav_args );

			?>

		</div>

		<div class="resp_nav_btn">
			<a href="#" class="slidex-init"></a>
		</div>

		<div class="sitenav-resp">
			<?php
			$sitenav_args = array(
				'theme_location'  => 'sitenav',
				'menu'            => '',
				'container'       => '',
				'container_class' => '',
				'container_id'    => '',
				'menu_class'      => 'sitenav-resp-list',
				'menu_id'         => '',
				'echo'            => true,
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'depth'           => 0,
				'walker'          => ''
			);

			wp_nav_menu( $sitenav_args );
			?>
		</div>

	</div>

</div>