<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( 'shop' ); ?>

<div class="main">
	<div class="container container-padding">

		<div class="row">

			<div class="half_column_last">

				<div class="event-full">

					<?php if( have_posts() ) :
						while( have_posts() ) :
							the_post() ;
							?>

							<?php
							$event_date = DateTime::createFromFormat('Ymd', get_field('event_date'));
							?>

							<div class="event-full-image">
								<?php the_post_thumbnail( 'event_img' ); ?>
							</div>

							<div class="event-full-meta">
								
								<div class="event-full-meta-head">
									<div class="event-full-meta-left">
										<h1><?php the_title(); ?></h1>
										<p class="event-full-meta-date"><?php echo $event_date->format('d M y'); ?></p>
									</div>
									<div class="event-full-meta-right">
									</div>
								</div>

								<div class="event-full-meta-body">

									<?php

									the_content();

									// wc_get_template_part( 'content', 'single-product' );

									?>

								</div>

								<div class="event-full-meta-cta">
									<a href="<?php the_field('event_fb_link'); ?>" class="btn btn-wide" target="_blank">Facebook event</a>
								</div>

							</div>

							<?php
						endwhile;
					endif; ?>

				</div>

			</div>

			<div class="half_column">

				<div class="main-events">

					<?php
					$curID = get_the_ID();
					?>

					<?php
					$coming_events_args = array(
						'post_type' => 'product',
						'posts_per_page' => 6
					);

					$coming_events = new WP_Query( $coming_events_args );

					if( $coming_events->have_posts() ) :
						while( $coming_events->have_posts() ) :
							$coming_events->the_post();
							
							$event_term = wp_get_post_terms( $post->ID, 'product_cat' ); ?>

							<?php
							//var_dump($event_term);
							if( $event_term[0]->term_id == 13 ) : // ILFP

							?><div class="main-event ilfp-event"><?php

							elseif( $event_term[0]->term_id == 5 ) : // LS

							?><div class="main-event ls-event"><?php

							elseif( $event_term[0]->term_id == 6 ) : // AW

							?><div class="main-event aw-event"><?php

							elseif( $event_term[0]->term_id == 8 ) : // L3

							?><div class="main-event l3-event"><?php

							else :

							?><div class="main-event"><?php

							endif;
							?>

								<div class="main-event-date">

								<?php
								$event_date = DateTime::createFromFormat('Ymd', get_field('event_date'));
								?>

									<div class="main-event-row">
										<div class="main-event-day">
											<?php if($event_date) : echo $event_date->format('d'); endif; ?>
										</div>

										<div class="main-event-date_wrapper">
											<div class="main-event-month">
												<?php if($event_date) : echo $event_date->format('M'); endif; ?>
											</div>
											<div class="main-event-year">
												<?php if($event_date) : echo $event_date->format('Y'); endif; ?>
											</div>
										</div>
									</div>

								</div>

								<div class="main-event-meta">

									<div class="main-event-title">
										<h2>
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										</h2>
									</div>

									<div class="main-event-rsvp">
										<a href="#" class="rsvp">RSVP</a>
									</div>

								</div>

								<?php
								if( $curID == $post->ID) :
									?>
									<span class="current-indicator">
									</span>
									<?php
								endif;
								?>
								

							</div>

							<?php

						endwhile;
					endif;

					wp_reset_query();

					?>

				</div>

				<div class="main-social">
			
					<?php get_template_part('content', 'social_list'); ?>

				</div>

			</div>

		</div>

	</div>
</div>

<?php get_footer( 'shop' ); ?>