<?php

/*-----------------------------------------------------------------------------------*/
/* Column Shortcodes */
/*-----------------------------------------------------------------------------------*/

function webtreats_one_third( $atts, $content = null ) {
return '<div class="one_third">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_third', 'webtreats_one_third');
 
function webtreats_one_third_last( $atts, $content = null ) {
return '<div class="one_third last">' . do_shortcode($content) . '</div><div class="clearboth"></div>';
}
add_shortcode('one_third_last', 'webtreats_one_third_last');
 
function webtreats_two_third( $atts, $content = null ) {
return '<div class="two_third">' . do_shortcode($content) . '</div>';
}
add_shortcode('two_third', 'webtreats_two_third');
 
function webtreats_two_third_last( $atts, $content = null ) {
return '<div class="two_third last">' . do_shortcode($content) . '</div><div class="clearboth"></div>';
}
add_shortcode('two_third_last', 'webtreats_two_third_last');
 
function webtreats_one_half( $atts, $content = null ) {
return '<div class="one_half">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_half', 'webtreats_one_half');
 
function webtreats_one_half_last( $atts, $content = null ) {
return '<div class="one_half last">' . do_shortcode($content) . '</div><div class="clearboth"></div>';
}
add_shortcode('one_half_last', 'webtreats_one_half_last');
 
function webtreats_one_fourth( $atts, $content = null ) {
return '<div class="one_fourth">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_fourth', 'webtreats_one_fourth');
 
function webtreats_one_fourth_last( $atts, $content = null ) {
return '<div class="one_fourth last">' . do_shortcode($content) . '</div><div class="clearboth"></div>';
}
add_shortcode('one_fourth_last', 'webtreats_one_fourth_last');
 
function webtreats_three_fourth( $atts, $content = null ) {
return '<div class="three_fourth">' . do_shortcode($content) . '</div>';
}
add_shortcode('three_fourth', 'webtreats_three_fourth');
 
function webtreats_three_fourth_last( $atts, $content = null ) {
return '<div class="three_fourth last">' . do_shortcode($content) . '</div><div class="clearboth"></div>';
}
add_shortcode('three_fourth_last', 'webtreats_three_fourth_last');
 
function webtreats_one_fifth( $atts, $content = null ) {
return '<div class="one_fifth">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_fifth', 'webtreats_one_fifth');
 
function webtreats_one_fifth_last( $atts, $content = null ) {
return '<div class="one_fifth last">' . do_shortcode($content) . '</div><div class="clearboth"></div>';
}
add_shortcode('one_fifth_last', 'webtreats_one_fifth_last');
 
function webtreats_two_fifth( $atts, $content = null ) {
return '<div class="two_fifth">' . do_shortcode($content) . '</div>';
}
add_shortcode('two_fifth', 'webtreats_two_fifth');
 
function webtreats_two_fifth_last( $atts, $content = null ) {
return '<div class="two_fifth last">' . do_shortcode($content) . '</div><div class="clearboth"></div>';
}
add_shortcode('two_fifth_last', 'webtreats_two_fifth_last');
 
function webtreats_three_fifth( $atts, $content = null ) {
return '<div class="three_fifth">' . do_shortcode($content) . '</div>';
}
add_shortcode('three_fifth', 'webtreats_three_fifth');
 
function webtreats_three_fifth_last( $atts, $content = null ) {
return '<div class="three_fifth last">' . do_shortcode($content) . '</div><div class="clearboth"></div>';
}
add_shortcode('three_fifth_last', 'webtreats_three_fifth_last');
 
function webtreats_four_fifth( $atts, $content = null ) {
return '<div class="four_fifth">' . do_shortcode($content) . '</div>';
}
add_shortcode('four_fifth', 'webtreats_four_fifth');
 
function webtreats_four_fifth_last( $atts, $content = null ) {
return '<div class="four_fifth last">' . do_shortcode($content) . '</div><div class="clearboth"></div>';
}
add_shortcode('four_fifth_last', 'webtreats_four_fifth_last');
 
function webtreats_one_sixth( $atts, $content = null ) {
return '<div class="one_sixth">' . do_shortcode($content) . '</div>';
}
add_shortcode('one_sixth', 'webtreats_one_sixth');
 
function webtreats_one_sixth_last( $atts, $content = null ) {
return '<div class="one_sixth last">' . do_shortcode($content) . '</div><div class="clearboth"></div>';
}
add_shortcode('one_sixth_last', 'webtreats_one_sixth_last');
 
function webtreats_five_sixth( $atts, $content = null ) {
return '<div class="five_sixth">' . do_shortcode($content) . '</div>';
}
add_shortcode('five_sixth', 'webtreats_five_sixth');
 
function webtreats_five_sixth_last( $atts, $content = null ) {
return '<div class="five_sixth last">' . do_shortcode($content) . '</div><div class="clearboth"></div>';
}
add_shortcode('five_sixth_last', 'webtreats_five_sixth_last');