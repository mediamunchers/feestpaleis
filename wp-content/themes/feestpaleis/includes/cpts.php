<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page' );
}
?>
<?php

/*-----------------------------------------------------------------------------------*/
/* Events
/*-----------------------------------------------------------------------------------*/

add_action( 'init', 'custom_post_type_events' );

function custom_post_type_events() {
		  
	$labels = array(
		'name' => __('Events', 'customtheme'),
		'singular_name' => __('Events', 'customtheme'),
		'add_new' => __('Add New', 'customtheme'), __('Event', 'customtheme'),
		'add_new_item' => __('Event', 'customtheme'),
		'edit_item' => __('Edit Event', 'customtheme'),
		'new_item' => __('New Event', 'customtheme'),
		'view_item' => __('View Event', 'customtheme'),
		'search_items' => __('Search Events', 'customtheme'),
		'not_found' =>  __('No Events found', 'customtheme'),
		'not_found_in_trash' => __('No Events found in Trash', 'customtheme'),
		'parent_item_colon' => ''
	);

	$supports = array(
		'title',
		'editor',
		'categories',
		'excerpt',
		'thumbnail'
	);

	$capabilities = array(
		'publish_posts' => 'manage_woocommerce',
        'edit_posts' => 'manage_woocommerce',
        'edit_others_posts' => 'manage_woocommerce',
        'delete_posts' => 'manage_woocommerce',
        'delete_others_posts' => 'manage_woocommerce',
        'read_private_posts' => 'manage_woocommerce',
        'edit_post' => 'manage_woocommerce',
        'delete_post' => 'manage_woocommerce',
        'read_post' => 'manage_woocommerce',
	);
  
	register_post_type( 'event',
		array(
			'labels' => $labels,
			'public' => true,
			'menu_position' => 5,
			'hierarchical' => false,
			'has_archive' => true,
			'menu_icon' => 'dashicons-calendar',
			'supports' => $supports,
			'capabilities' => $capabilities
		)
	);
	
}


// Custom Taxonomy
add_action( 'init', 'custom_taxonomy_eventConcepts', 0 );  

function custom_taxonomy_eventConcepts() {
		
	// Recipe Type Custom Taxonomy
	$recipe_type_labels = array(
	    'name' => __('Concepts', 'customtheme'),
	    'singular_name' => __('Concept', 'customtheme'),
	    'search_items' => __('Search Concepts', 'customtheme'),
	    'all_items' => __('All Concepts', 'customtheme'),
	    'parent_item' => __('Parent Concept', 'customtheme'),
	    'parent_item_colon' =>__('Parent Concept:', 'customtheme'),
	    'edit_item' => __('Edit Concept', 'customtheme'), 
	    'update_item' => __('Update Concept', 'customtheme'),
	    'add_new_item' => __('Add Concept', 'customtheme'),
	    'new_item_name' => __('Concept Name', 'customtheme'),
	    'menu_name' => __('Concepts', 'customtheme')
	  ); 


	register_taxonomy(
	    'event_concepts',
	    array( 'event' ),
	    array(
			  'hierarchical' => true,
			  'labels' => $recipe_type_labels,
			  'query_var' => true,
	    )
	);
}



/*-----------------------------------------------------------------------------------*/
/* GALLERIES
/*-----------------------------------------------------------------------------------*/

add_action( 'init', 'custom_post_type_galleries' );

function custom_post_type_galleries() {
		  
	$labels = array(
		'name' => __('Galleries', 'customtheme'),
		'singular_name' => __('Galleries', 'customtheme'),
		'add_new' => __('Add New', 'customtheme'), __('Gallery', 'customtheme'),
		'add_new_item' => __('Gallery', 'customtheme'),
		'edit_item' => __('Edit Gallery', 'customtheme'),
		'new_item' => __('New Gallery', 'customtheme'),
		'view_item' => __('View Gallery', 'customtheme'),
		'search_items' => __('Search Galleries', 'customtheme'),
		'not_found' =>  __('No Galleries found', 'customtheme'),
		'not_found_in_trash' => __('No Galleries found in Trash', 'customtheme'),
		'parent_item_colon' => ''
	);

	$supports = array(
		'title',
		'editor',
		'categories',
		'excerpt',
		'thumbnail'
	);

	$capabilities = array(
		'publish_posts' => 'manage_woocommerce',
        'edit_posts' => 'manage_woocommerce',
        'edit_others_posts' => 'manage_woocommerce',
        'delete_posts' => 'manage_woocommerce',
        'delete_others_posts' => 'manage_woocommerce',
        'read_private_posts' => 'manage_woocommerce',
        'edit_post' => 'manage_woocommerce',
        'delete_post' => 'manage_woocommerce',
        'read_post' => 'manage_woocommerce',
	);
  
	register_post_type( 'gallery',
		array(
			'labels' => $labels,
			'public' => true,
			'menu_position' => 5,
			'hierarchical' => false,
			'has_archive' => true,
			'menu_icon' => 'dashicons-format-gallery',
			'supports' => $supports,
			'capabilities' => $capabilities
		)
	);
	
}

?>