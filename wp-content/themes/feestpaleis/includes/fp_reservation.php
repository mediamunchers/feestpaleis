<?php

add_action( 'pre_get_posts', 'fp_custom_create_booking' );
function fp_custom_create_booking( $query ){

	if( isset($_POST['res_step3_token']) && $_POST['res_step3_token'] == 12 && !is_admin() && $query->query_vars['page_id'] == 146 ){

		// Gather order values
		$res_persons = $_POST['res_val_persons'];

		if( isset( $_POST['res_val_bub1'] ) ){
			$res_bubbles1 = $_POST['res_val_bub1'];
		}
		if( isset( $_POST['res_val_bub2'] ) ){
			$res_bubbles2 = $_POST['res_val_bub2'];
		}
		if( isset( $_POST['res_val_bub3'] ) ){
			$res_bubbles3 = $_POST['res_val_bub3'];
		}
		if( isset( $_POST['res_val_bub4'] ) ){
			$res_bubbles4 = $_POST['res_val_bub4'];
		}
		if( isset( $_POST['res_val_bub5'] ) ){
			$res_bubbles5 = $_POST['res_val_bub5'];
		}
		if( isset( $_POST['res_val_bub6'] ) ){
			$res_bubbles6 = $_POST['res_val_bub6'];
		}
		if( isset( $_POST['res_val_bub7'] ) ){
			$res_bubbles7 = $_POST['res_val_bub7'];
		}
		if( isset( $_POST['res_val_bub8'] ) ){
			$res_bubbles8 = $_POST['res_val_bub8'];
		}
		if( isset( $_POST['res_val_bub9'] ) ){
			$res_bubbles9 = $_POST['res_val_bub9'];
		}
		if( isset( $_POST['res_val_bub10'] ) ){
			$res_bubbles10 = $_POST['res_val_bub10'];
		}
		if( isset( $_POST['res_val_bub11'] ) ){
			$res_bubbles11 = $_POST['res_val_bub11'];
		}
		if( isset( $_POST['res_val_bub12'] ) ){
			$res_bubbles12 = $_POST['res_val_bub12'];
		}
		if( isset( $_POST['res_val_bub13'] ) ){
			$res_bubbles13 = $_POST['res_val_bub13'];
		}
		if( isset( $_POST['res_val_bub14'] ) ){
			$res_bubbles14 = $_POST['res_val_bub14'];
		}
		if( isset( $_POST['res_val_bub15'] ) ){
			$res_bubbles15 = $_POST['res_val_bub15'];
		}
		if( isset( $_POST['res_val_bub16'] ) ){
			$res_bubbles16 = $_POST['res_val_bub16'];
		}
		if( isset( $_POST['res_val_bub17'] ) ){
			$res_bubbles17 = $_POST['res_val_bub17'];
		}
		if( isset( $_POST['res_val_bub18'] ) ){
			$res_bubbles18 = $_POST['res_val_bub18'];
		}
		if( isset( $_POST['res_val_bub19'] ) ){
			$res_bubbles19 = $_POST['res_val_bub19'];
		}
		if( isset( $_POST['res_val_bub20'] ) ){
			$res_bubbles20 = $_POST['res_val_bub20'];
		}

		if( isset( $_POST['res_val_str1'] ) ){
			$res_strong1 = $_POST['res_val_str1'];
		}
		if( isset( $_POST['res_val_str2'] ) ){
			$res_strong2 = $_POST['res_val_str2'];
		}
		if( isset( $_POST['res_val_str3'] ) ){
			$res_strong3 = $_POST['res_val_str3'];
		}
		if( isset( $_POST['res_val_str4'] ) ){
			$res_strong4 = $_POST['res_val_str4'];
		}
		if( isset( $_POST['res_val_str5'] ) ){
			$res_strong5 = $_POST['res_val_str5'];
		}
		if( isset( $_POST['res_val_str6'] ) ){
			$res_strong6 = $_POST['res_val_str6'];
		}
		if( isset( $_POST['res_val_str7'] ) ){
			$res_strong7 = $_POST['res_val_str7'];
		}
		if( isset( $_POST['res_val_str8'] ) ){
			$res_strong8 = $_POST['res_val_str8'];
		}
		if( isset( $_POST['res_val_str9'] ) ){
			$res_strong9 = $_POST['res_val_str9'];
		}
		if( isset( $_POST['res_val_str10'] ) ){
			$res_strong10 = $_POST['res_val_str10'];
		}
		if( isset( $_POST['res_val_str11'] ) ){
			$res_strong11 = $_POST['res_val_str11'];
		}
		if( isset( $_POST['res_val_str12'] ) ){
			$res_strong12 = $_POST['res_val_str12'];
		}
		if( isset( $_POST['res_val_str13'] ) ){
			$res_strong13 = $_POST['res_val_str13'];
		}
		if( isset( $_POST['res_val_str14'] ) ){
			$res_strong14 = $_POST['res_val_str14'];
		}
		if( isset( $_POST['res_val_str15'] ) ){
			$res_strong15 = $_POST['res_val_str15'];
		}
		if( isset( $_POST['res_val_str16'] ) ){
			$res_strong16 = $_POST['res_val_str16'];
		}
		if( isset( $_POST['res_val_str17'] ) ){
			$res_strong17 = $_POST['res_val_str17'];
		}
		if( isset( $_POST['res_val_str18'] ) ){
			$res_strong18 = $_POST['res_val_str18'];
		}
		if( isset( $_POST['res_val_str19'] ) ){
			$res_strong19 = $_POST['res_val_str19'];
		}
		if( isset( $_POST['res_val_str20'] ) ){
			$res_strong20 = $_POST['res_val_str20'];
		}

		if( isset( $_POST['res_val_sof1'] ) ){
			$res_soft1 = $_POST['res_val_sof1'];
		}
		if( isset( $_POST['res_val_sof2'] ) ){
			$res_soft2 = $_POST['res_val_sof2'];
		}
		if( isset( $_POST['res_val_sof3'] ) ){
			$res_soft3 = $_POST['res_val_sof3'];
		}
		if( isset( $_POST['res_val_sof4'] ) ){
			$res_soft4 = $_POST['res_val_sof4'];
		}
		if( isset( $_POST['res_val_sof5'] ) ){
			$res_soft5 = $_POST['res_val_sof5'];
		}


		if( isset( $_POST['res_val_extras'] ) ){
			$res_extras = $_POST['res_val_extras'];
		} else {
			$res_extras = 'none';
		}

		$res_date = $_POST['res_val_date'];
		$res_total = $_POST['res_val_total'];

		$res_name = $_POST['res_val_name'];
		$res_lastName = $_POST['res_val_lastName'];
		$res_email = $_POST['res_val_email'];
		$res_tel = $_POST['res_val_tel'];
		$res_street = $_POST['res_val_street'];
		$res_housenr = $_POST['res_val_housenr'];
		$res_postal = $_POST['res_val_postal'];
		$res_city = $_POST['res_val_city'];

		/*-------------------------------------------------------*/
		/* CREATE USER */
		/*-------------------------------------------------------*/
		if( null == username_exists( $res_email ) ) {

		    $userdata = array(
		    	'user_login' => $res_email,
				'user_pass'=> wp_generate_password ( 12, false ),
				'user_email'=>$res_email,
				'role' => 'website-reservation'
			);

			$user_id = wp_insert_user( $userdata ) ;

			$user_id = wp_update_user( 
				array( 
					'ID' => $user_id,
					//'user_url' => $website,
					'user_nicename' => $res_name,
					//'user_url' => $conf_reg_website,
					'user_email' => $res_email,
					'display_name' => $res_name,
					'nickname' => $res_name,
					'first_name' => $res_name,
					'last_name' => $res_lastName,
					'user_registered' => date("Ymd"),
					'show_admin_bar_front' => false
				)
			);

			// update_user_meta( $user_id, 'user_conf_id', $conf_id );
			// update_user_meta( $user_id, 'title', $conf_reg_title );
			// update_user_meta( $user_id, 'function', $conf_reg_function );

			// On success
			// if( !is_wp_error($user_id) ) {
			// 	wp_redirect( get_permalink( 15 ) );
			// }

		} // End if user NOT exists


		$user = get_user_by( 'email', $res_email );


		$address = array(
            'first_name' => $res_name,
            'last_name'  => $res_lastName,
            'company'    => '',
            'email'      => $res_email,
            'phone'      => $res_tel,
            'address_1'  => $res_housenr . ' ' . $res_street,
            'address_2'  => '', 
            'city'       => $res_city,
            'state'      => 'CT',
            'postcode'   => $res_postal,
            'country'    => 'BE'
        );

        $custom_order_args = array(
			'status'        => '',
			'customer_id'   => $user->ID,
			'customer_note' => 'VIP Tafel ' . $res_persons . ' personen
								Optie: ' . $res_extras . '
								Datum: ' . $res_date,
			'order_id'      => 0
		);

        $order = wc_create_order( $custom_order_args );
        if( $res_bubbles1 != "" ){
        	$order->add_product( get_product( $res_bubbles1 ), 1 );
        	$res_bubbles1_title = "- ".get_the_title($res_bubbles1)."<br/>";
        }
        if( $res_bubbles2 != "" ){
        	$order->add_product( get_product( $res_bubbles2 ), 1 );
        	$res_bubbles2_title = "- ".get_the_title($res_bubbles2)."<br/>";
        }
        if( $res_bubbles3 != "" ){
        	$order->add_product( get_product( $res_bubbles3 ), 1 );
        	$res_bubbles3_title = "- ".get_the_title($res_bubbles3)."<br/>";
        }
        if( $res_bubbles4 != "" ){
        	$order->add_product( get_product( $res_bubbles4 ), 1 );
        	$res_bubbles4_title = "- ".get_the_title($res_bubbles4)."<br/>";
        }
        if( $res_bubbles5 != "" ){
        	$order->add_product( get_product( $res_bubbles5 ), 1 );
        	$res_bubbles5_title = "- ".get_the_title($res_bubbles5)."<br/>";
        }
        if( $res_bubbles6 != "" ){
        	$order->add_product( get_product( $res_bubbles6 ), 1 );
        	$res_bubbles6_title = "- ".get_the_title($res_bubbles6)."<br/>";
        }
        if( $res_bubbles7 != "" ){
        	$order->add_product( get_product( $res_bubbles7 ), 1 );
        	$res_bubbles7_title = "- ".get_the_title($res_bubbles7)."<br/>";
        }
        if( $res_bubbles8 != "" ){
        	$order->add_product( get_product( $res_bubbles8 ), 1 );
        	$res_bubbles8_title = "- ".get_the_title($res_bubbles8)."<br/>";
        }
        if( $res_bubbles9 != "" ){
        	$order->add_product( get_product( $res_bubbles9 ), 1 );
        	$res_bubbles9_title = "- ".get_the_title($res_bubbles9)."<br/>";
        }
        if( $res_bubbles10 != "" ){
        	$order->add_product( get_product( $res_bubbles10 ), 1 );
        	$res_bubbles10_title = "- ".get_the_title($res_bubbles10)."<br/>";
        }
        if( $res_bubbles11 != "" ){
        	$order->add_product( get_product( $res_bubbles11 ), 1 );
        	$res_bubbles11_title = "- ".get_the_title($res_bubbles11)."<br/>";
        }
        if( $res_bubbles12 != "" ){
        	$order->add_product( get_product( $res_bubbles12 ), 1 );
        	$res_bubbles12_title = "- ".get_the_title($res_bubbles12)."<br/>";
        }
        if( $res_bubbles13 != "" ){
        	$order->add_product( get_product( $res_bubbles13 ), 1 );
        	$res_bubbles13_title = "- ".get_the_title($res_bubbles13)."<br/>";
        }
        if( $res_bubbles14 != "" ){
        	$order->add_product( get_product( $res_bubbles14 ), 1 );
        	$res_bubbles14_title = "- ".get_the_title($res_bubbles14)."<br/>";
        }
        if( $res_bubbles15 != "" ){
        	$order->add_product( get_product( $res_bubbles15 ), 1 );
        	$res_bubbles15_title = "- ".get_the_title($res_bubbles15)."<br/>";
        }
        if( $res_bubbles16 != "" ){
        	$order->add_product( get_product( $res_bubbles16 ), 1 );
        	$res_bubbles16_title = "- ".get_the_title($res_bubbles16)."<br/>";
        }
        if( $res_bubbles17 != "" ){
        	$order->add_product( get_product( $res_bubbles17 ), 1 );
        	$res_bubbles17_title = "- ".get_the_title($res_bubbles17)."<br/>";
        }
        if( $res_bubbles18 != "" ){
        	$order->add_product( get_product( $res_bubbles18 ), 1 );
        	$res_bubbles18_title = "- ".get_the_title($res_bubbles18)."<br/>";
        }
        if( $res_bubbles19 != "" ){
        	$order->add_product( get_product( $res_bubbles19 ), 1 );
        	$res_bubbles19_title = "- ".get_the_title($res_bubbles19)."<br/>";
        }
        if( $res_bubbles20 != "" ){
        	$order->add_product( get_product( $res_bubbles20 ), 1 );
        	$res_bubbles20_title = "- ".get_the_title($res_bubbles20)."<br/>";
        }

        if( $res_strong1 != "" ){
        	$order->add_product( get_product( $res_strong1 ), 1 );
        	$res_strong1_title = "- ".get_the_title($res_strong1)."<br/>";
        }
        if( $res_strong2 != "" ){
        	$order->add_product( get_product( $res_strong2 ), 1 );
        	$res_strong2_title = "- ".get_the_title($res_strong2)."<br/>";
        }
        if( $res_strong3 != "" ){
        	$order->add_product( get_product( $res_strong3 ), 1 );
        	$res_strong3_title = "- ".get_the_title($res_strong3)."<br/>";
        }
        if( $res_strong4 != "" ){
        	$order->add_product( get_product( $res_strong4 ), 1 );
        	$res_strong4_title = "- ".get_the_title($res_strong4)."<br/>";
        }
        if( $res_strong5 != "" ){
        	$order->add_product( get_product( $res_strong5 ), 1 );
        	$res_strong5_title = "- ".get_the_title($res_strong5)."<br/>";
        }
        if( $res_strong6 != "" ){
        	$order->add_product( get_product( $res_strong6 ), 1 );
        	$res_strong6_title = "- ".get_the_title($res_strong6)."<br/>";
        }
        if( $res_strong7 != "" ){
        	$order->add_product( get_product( $res_strong7 ), 1 );
        	$res_strong7_title = "- ".get_the_title($res_strong7)."<br/>";
        }
        if( $res_strong8 != "" ){
        	$order->add_product( get_product( $res_strong8 ), 1 );
        	$res_strong8_title = "- ".get_the_title($res_strong8)."<br/>";
        }
        if( $res_strong9 != "" ){
        	$order->add_product( get_product( $res_strong9 ), 1 );
        	$res_strong9_title = "- ".get_the_title($res_strong9)."<br/>";
        }
        if( $res_strong10 != "" ){
        	$order->add_product( get_product( $strong_bubbles10 ), 1 );
        	$res_strong10_title = "- ".get_the_title($res_strong10)."<br/>";
        }
        if( $res_strong11 != "" ){
        	$order->add_product( get_product( $strong_bubbles11 ), 1 );
        	$res_strong11_title = "- ".get_the_title($res_strong11)."<br/>";
        }
        if( $res_strong12 != "" ){
        	$order->add_product( get_product( $strong_bubbles12 ), 1 );
        	$res_strong12_title = "- ".get_the_title($res_strong12)."<br/>";
        }
        if( $res_strong13 != "" ){
        	$order->add_product( get_product( $strong_bubbles13 ), 1 );
        	$res_strong13_title = "- ".get_the_title($res_strong13)."<br/>";
        }
        if( $res_strong14 != "" ){
        	$order->add_product( get_product( $strong_bubbles14 ), 1 );
        	$res_strong14_title = "- ".get_the_title($res_strong14)."<br/>";
        }
        if( $res_strong15 != "" ){
        	$order->add_product( get_product( $strong_bubbles15 ), 1 );
        	$res_strong15_title = "- ".get_the_title($res_strong15)."<br/>";
        }
        if( $res_strong16 != "" ){
        	$order->add_product( get_product( $strong_bubbles16 ), 1 );
        	$res_strong16_title = "- ".get_the_title($res_strong16)."<br/>";
        }
        if( $res_strong17 != "" ){
        	$order->add_product( get_product( $strong_bubbles17 ), 1 );
        	$res_strong17_title = "- ".get_the_title($res_strong17)."<br/>";
        }
        if( $res_strong18 != "" ){
        	$order->add_product( get_product( $strong_bubbles18 ), 1 );
        	$res_strong18_title = "- ".get_the_title($res_strong18)."<br/>";
        }
        if( $res_strong19 != "" ){
        	$order->add_product( get_product( $strong_bubbles19 ), 1 );
        	$res_strong19_title = "- ".get_the_title($res_strong19)."<br/>";
        }
        if( $res_strong20 != "" ){
        	$order->add_product( get_product( $strong_bubbles20 ), 1 );
        	$res_strong20_title = "- ".get_the_title($res_strong20)."<br/>";
        }

        if( $res_soft1 != "" ){
    	   	$order->add_product( get_product( $res_soft1 ), 1 );
        }
        if( $res_soft2 != "" ){
        	$order->add_product( get_product( $res_soft2 ), 1 );
        }
        if( $res_soft3 != "" ){
        	$order->add_product( get_product( $res_soft3 ), 1 );
        }
        if( $res_soft4 != "" ){
        	$order->add_product( get_product( $res_soft4 ), 1 );
        }
        if( $res_soft5 != "" ){
        	$order->add_product( get_product( $res_soft5 ), 1 );
        }

        $order->set_address( $address, 'billing' );
        $order->set_address( $address, 'shipping' );
        $order->set_total( $res_total, 'total' );
        //$order->calculate_totals();

        //wp_mail( 'admin@example.com', $subject, $message );
        $subject_client = "Bedankt voor uw reservatie bij Danstheater Feestpaleis";
        $message_client = 'Beste '.$res_name.',<br/><br/>Wij hebben uw reservatie goed ontvangen en zullen deze zo snel mogelijk verwerken.<br/><br/>Met vriendelijke groeten,<br/>Danstheater Feestpaleis';

        $subject_fp = "Nieuwe VIP Tafel reservatie";
        $message_fp = 'Aantal personen: '.$res_persons.'<br/>Datum: '.$res_date.'<br/><br/>Naam: '.$res_name.' '.$res_lastName.'<br/>E-mail: '.$res_email.'<br/>Telefoon: '.$res_tel.'<br/><br/>Adres: '.$res_street.' '.$res_housenr.', '.$res_postal.' '.$res_city.'<br/><br/>Gekozen dranken:<br/>'.$res_bubbles1_title.$res_bubbles2_title.$res_bubbles3_title.$res_bubbles4_title.$res_bubbles5_title.$res_bubbles6_title.$res_bubbles7_title.$res_bubbles8_title.$res_bubbles9_title.$res_bubbles10_title.$res_bubbles11_title.$res_bubbles12_title.$res_bubbles13_title.$res_bubbles14_title.$res_bubbles15_title.$res_bubbles16_title.$res_bubbles17_title.$res_bubbles18_title.$res_bubbles19_title.$res_bubbles20_title.$res_strong1_title.$res_strong2_title.$res_strong3_title.$res_strong4_title.$res_strong5_title.$res_strong6_title.$res_strong7_title.$res_strong8_title.$res_strong9_title.$res_strong10_title.$res_strong11_title.$res_strong12_title.$res_strong13_title.$res_strong14_title.$res_strong15_title.$res_strong16_title.$res_strong17_title.$res_strong18_title.$res_strong19_title.$res_strong20_title;

        add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
        wp_mail( $res_email, $subject_client, $message_client );
        wp_mail( "website@feestpaleis.be", $subject_fp, $message_fp );

        wp_redirect( get_permalink( 152 )  ); exit;


	} // Endif submission

	//wp_redirect( get_permalink(152) );

}


?>