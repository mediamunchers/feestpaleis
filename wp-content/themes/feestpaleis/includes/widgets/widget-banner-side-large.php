<?php

/*-----------------------------------------------------------------------------------*/
/* Banner Side Small Widget
/*-----------------------------------------------------------------------------------*/

class Banner_Widget_Side_Large extends WP_Widget {
	
	function Banner_Widget_Side_Large(){
	  		$widget_ops = array( 'classname' => 'Banner_Widget_Side_Large', 'description' => __('Side Banner Large', 'woothemes'));
			$this->WP_Widget( 'banner_side_large', __('Banner Side Large', 'woothemes'), $widget_ops );
	}

/********* Starting Banner Side Small Widget Function *********/
	
	function widget($args,  $instance) 
	{
		extract($args);
		
		$title = apply_filters('widget_title', $instance['title']);		
		if ( empty($title) ) 
				$title = false;
			echo $before_widget;
		
		
		

		if($title):
			
			$temp_title = explode(' ',$title);
			$first_letter = $temp_title[0];
					unset($temp_title[0]);
			$title_new = implode(' ', $temp_title);
			$title = $first_letter.'  '.$title_new.' ';
			echo '<h3>';
					echo $title;
			echo '</h3>';	
		
		endif;
		
	
		$args = array(
			'post_type'			=> 'banner',
			'post_per_age'		=> '1',
			'banner_locations'	=> 'side_large'
		);

		$banner = new WP_Query( $args );

		if( $banner->have_posts() ) :
			while( $banner->have_posts() ) : $banner->the_post();
			?>

			<?php the_post_thumbnail('full'); ?>

			<?php
			endwhile;
		endif;

		wp_reset_query();


	echo $after_widget;
	}
	

/********* Starting Banner Side Small Widget Admin Form *********/

	function form($instance) 
	{	
		$instance = wp_parse_args( (array) $instance, array() );
	
        $title= esc_attr($instance['title']);

		?>
			<p>
	            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title', 'woothemes'); ?></label>
	            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
	        </p>
		<?php
	}
	
/********* Starting Banner Side Small Widget Update Function *********/

	function update($new_instance, $old_instance) 
	{
        $instance=$old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		
        return $instance;

    }
	
}
register_widget( 'Banner_Widget_Side_Large' );