<?php
/*
Template name: Events
*/
?>

<?php get_header(); ?>

<div class="main">
	<div class="container container-padding-top">

		<div class="row">

			<div class="half_column">

				<div class="main-events">

					<?php
					$now = (date('U') - 86400) * 1000;
					//Display 10 coming events
					$coming_events_args = array(
						'post_type' => 'event',
						'posts_per_page' => 8,
			            'meta_key' => 'event_date',
			            'meta_query' => array(
							array(
								'key'     => 'featured_event',
								'value'   => 'false',
								'compare' => '='
							),
							array(
								'key'     => 'event_date',
								'value'   => $now,
								'compare' => '>='
							)
						),
			            'orderby' => 'meta_value',
			            'order' => 'ASC'
					);

					$coming_events = new WP_Query( $coming_events_args );

					if( $coming_events->have_posts() ) :

						$ii = 0;

						while( $coming_events->have_posts() ) :
							$coming_events->the_post();

							if( $ii == 5 ){ ?>
								</div>
											
							</div>

							<div class="half_column_last">

								<div class="main-events">
							<?php } ?>
				
							<?php get_template_part('content', 'event'); ?>

						<?php

						$ii++;

						endwhile;

					endif;

					//Display 2 featured events
					$featured_events_args = array(
						'post_type' => 'event',
						'posts_per_page' => 2,
			            'meta_key' => 'event_date',
			            'meta_query' => array(
							array(
								'key'     => 'featured_event',
								'value'   => 'true',
								'compare' => '='
							),
							array(
								'key'     => 'event_date',
								'value'   => $now,
								'compare' => '>='
							)
						),
			            'orderby' => 'meta_value',
			            'order' => 'ASC'
					);

					$featured_events = new WP_Query( $featured_events_args );

					if( $featured_events->have_posts() ) :

						while( $featured_events->have_posts() ) :
							$featured_events->the_post();

				
							get_template_part('content', 'event');

						endwhile;

					endif;
					?>				

				</div>

			</div>

		</div>

		<div class="container main-social">
			
			<?php get_template_part('content', 'social_list'); ?>

		</div>

	</div>

	<div class="main-gallery">
		<div class="container">

			<?php
			if( have_posts () ) :
				while( have_posts() ) :
					the_post();

					$gallery_images = get_field('home_gallery', 39);

					if( $gallery_images ): ?>

						<ul class="main-gallery-grid-full">

						<?php $gallery_counter = 0; ?>

						<?php foreach( $gallery_images as $img ): ?>

							<li>

								<a class="fancybox" rel="home_gallery" href="<?php echo $img['url']; ?>">
									<img src="<?php echo $img['sizes']['thumbnail']; ?>" alt="<?php echo $img['alt']; ?>" />
									<span class="main-gallery-overlay"></span>
								</a>

							</li>

							<?php if (++$gallery_counter == 16) break; ?>

						<?php endforeach; ?>

					</ul>

					<?php endif;

				endwhile;

			endif;
			?>

		</div>
	</div>

</div>

<?php get_footer(); ?>