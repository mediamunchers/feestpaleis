<?php get_header(); ?>

<div class="main">
	<div class="container container-padding">

	<?php if( have_posts() ) :
		while( have_posts() ) :
			the_post();
		?>

			<?php get_template_part('content', 'page' ); ?>

		<?php
		endwhile;
	else :
		?>
			<?php get_template_part('content', '404' ); ?>

		<?php endif;
	?>

	</div>
</div>

<?php get_footer(); ?>