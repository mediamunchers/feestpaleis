<?php
/*
Template name: Reservations Checkout
*/
?>

<?php 
if( $_POST['res2_persons'] && ( $_POST['res_val_bub1'] or $_POST['res_val_str1'] or $_POST['res_val_sof1'] ) ) :
	
	$res_persons = $_POST['res2_persons'];

	$res_bubbles1 = $_POST['res_val_bub1'];
	if( isset( $_POST['res_val_bub2'] ) ){
		$res_bubbles2 = $_POST['res_val_bub2'];
	}
	if( isset( $_POST['res_val_bub3'] ) ){
		$res_bubbles3 = $_POST['res_val_bub3'];
	}
	if( isset( $_POST['res_val_bub4'] ) ){
		$res_bubbles4 = $_POST['res_val_bub4'];
	}
	if( isset( $_POST['res_val_bub5'] ) ){
		$res_bubbles5 = $_POST['res_val_bub5'];
	}
	if( isset( $_POST['res_val_bub6'] ) ){
		$res_bubbles6 = $_POST['res_val_bub6'];
	}
	if( isset( $_POST['res_val_bub7'] ) ){
		$res_bubbles7 = $_POST['res_val_bub7'];
	}
	if( isset( $_POST['res_val_bub8'] ) ){
		$res_bubbles8 = $_POST['res_val_bub8'];
	}
	if( isset( $_POST['res_val_bub9'] ) ){
		$res_bubbles9 = $_POST['res_val_bub9'];
	}
	if( isset( $_POST['res_val_bub10'] ) ){
		$res_bubbles10 = $_POST['res_val_bub10'];
	}
	if( isset( $_POST['res_val_bub11'] ) ){
		$res_bubbles11 = $_POST['res_val_bub11'];
	}
	if( isset( $_POST['res_val_bub12'] ) ){
		$res_bubbles12 = $_POST['res_val_bub12'];
	}
	if( isset( $_POST['res_val_bub13'] ) ){
		$res_bubbles13 = $_POST['res_val_bub13'];
	}
	if( isset( $_POST['res_val_bub14'] ) ){
		$res_bubbles14 = $_POST['res_val_bub14'];
	}
	if( isset( $_POST['res_val_bub15'] ) ){
		$res_bubbles15 = $_POST['res_val_bub15'];
	}
	if( isset( $_POST['res_val_bub16'] ) ){
		$res_bubbles16 = $_POST['res_val_bub16'];
	}
	if( isset( $_POST['res_val_bub17'] ) ){
		$res_bubbles17 = $_POST['res_val_bub17'];
	}
	if( isset( $_POST['res_val_bub18'] ) ){
		$res_bubbles18 = $_POST['res_val_bub18'];
	}
	if( isset( $_POST['res_val_bub19'] ) ){
		$res_bubbles19 = $_POST['res_val_bub19'];
	}
	if( isset( $_POST['res_val_bub20'] ) ){
		$res_bubbles20 = $_POST['res_val_bub20'];
	}

	$res_strong1 = $_POST['res_val_str1'];
	if( isset( $_POST['res_val_str2'] ) ){
		$res_strong2 = $_POST['res_val_str2'];
	}
	if( isset( $_POST['res_val_str3'] ) ){
		$res_strong3 = $_POST['res_val_str3'];
	}
	if( isset( $_POST['res_val_str4'] ) ){
		$res_strong4 = $_POST['res_val_str4'];
	}
	if( isset( $_POST['res_val_str5'] ) ){
		$res_strong5 = $_POST['res_val_str5'];
	}
	if( isset( $_POST['res_val_str6'] ) ){
		$res_strong6 = $_POST['res_val_str6'];
	}
	if( isset( $_POST['res_val_str7'] ) ){
		$res_strong7 = $_POST['res_val_str7'];
	}
	if( isset( $_POST['res_val_str8'] ) ){
		$res_strong8 = $_POST['res_val_str8'];
	}
	if( isset( $_POST['res_val_str9'] ) ){
		$res_strong9 = $_POST['res_val_str9'];
	}
	if( isset( $_POST['res_val_str10'] ) ){
		$res_strong10 = $_POST['res_val_str10'];
	}
	if( isset( $_POST['res_val_str11'] ) ){
		$res_strong11 = $_POST['res_val_str11'];
	}
	if( isset( $_POST['res_val_str12'] ) ){
		$res_strong12 = $_POST['res_val_str12'];
	}
	if( isset( $_POST['res_val_str13'] ) ){
		$res_strong13 = $_POST['res_val_str13'];
	}
	if( isset( $_POST['res_val_str14'] ) ){
		$res_strong14 = $_POST['res_val_str14'];
	}
	if( isset( $_POST['res_val_str15'] ) ){
		$res_strong15 = $_POST['res_val_str15'];
	}
	if( isset( $_POST['res_val_str16'] ) ){
		$res_strong16 = $_POST['res_val_str16'];
	}
	if( isset( $_POST['res_val_str17'] ) ){
		$res_strong17 = $_POST['res_val_str17'];
	}
	if( isset( $_POST['res_val_str18'] ) ){
		$res_strong18 = $_POST['res_val_str18'];
	}
	if( isset( $_POST['res_val_str19'] ) ){
		$res_strong19 = $_POST['res_val_str19'];
	}
	if( isset( $_POST['res_val_str20'] ) ){
		$res_strong20 = $_POST['res_val_str20'];
	}

	$res_soft1 = $_POST['res_val_sof1'];
	if( isset( $_POST['res_val_sof2'] ) ){
		$res_soft2 = $_POST['res_val_sof2'];
	}
	if( isset( $_POST['res_val_sof3'] ) ){
		$res_soft3 = $_POST['res_val_sof3'];
	}
	if( isset( $_POST['res_val_sof4'] ) ){
		$res_soft4 = $_POST['res_val_sof4'];
	}
	if( isset( $_POST['res_val_sof5'] ) ){
		$res_soft5 = $_POST['res_val_sof5'];
	}

	$res_extras = $_POST['res_extras'];
	$res_date = $_POST['res_val_date'];
	$res_total = $_POST['res2_total'];

else :
	wp_redirect( get_permalink( 15 ) );
	exit;
endif;

get_header(); ?>

<div class="main">
	<div class="container container-padding">

		<div class="half_column_last reservations-content">

			<div class="reservations-thumb">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/reservation_vip_tafel.jpg" alt="" />
				<ul>
					<li><a href="<?php echo get_permalink(15); ?>" class="active">VIP Tafel</a></li>
					<li><a href="<?php echo get_permalink(391); ?>">Limo service</a></li>
					<li><a href="<?php echo get_permalink(450); ?>">Groepen</a></li>
				</ul>
			</div>

			<div class="reservations-container">

				<div class="reservations-container-inner">

					<?php /*
					<pre>
					<?php var_dump($wp_query); ?>
					</pre>
					*/ ?>

					<?php /*
					echo $res_persons . '<br />';
					echo $res_bubbles1 . '<br />';
					echo $res_bubbles2 . '<br />';
					echo $res_bubbles3 . '<br />';
					echo $res_bubbles4 . '<br />';
					echo $res_bubbles5 . '<br />';
					echo $res_strong1 . '<br />';
					echo $res_strong2 . '<br />';
					echo $res_strong3 . '<br />';
					echo $res_strong4 . '<br />';
					echo $res_strong5 . '<br />';
					echo $res_soft1 . '<br />';
					echo $res_soft2 . '<br />';
					echo $res_soft3 . '<br />';
					echo $res_soft4 . '<br />';
					echo $res_soft5 . '<br />';
					echo $res_extras . '<br />';
					echo $res_date . '<br />';
					echo $res_total . '<br />';
					*/ ?>

					<h5 class="color-main">Personal details</h5>
					<p>Vul hieronder enkel nog uw persoonlijke gegevens in om deze reservatie te voltooien.</p>

					<?php
					// echo $res_val_persons;
					// echo $res_val_food;
					// echo $res_val_date;
					// echo $res_val_drinks1;

					// $product          = get_product( 150 );
					// var_dump($product);
					?>

					<div class="reservations-checkout">
							
						<form method="post" action="" id="res_step3">

							<input type="hidden" value="12" name="res_step3_token">
							<input type="hidden" value="<?php echo $res_persons; ?>" name="res_val_persons" />

							<?php if( $res_bubbles1 ){
								?><input type="hidden" value="<?php echo $res_bubbles1; ?>" name="res_val_bub1" /><?php
							}?>
							<?php if( $res_bubbles2 ){
								?><input type="hidden" value="<?php echo $res_bubbles2; ?>" name="res_val_bub2" /><?php
							}?>
							<?php if( $res_bubbles3 ){
								?><input type="hidden" value="<?php echo $res_bubbles3; ?>" name="res_val_bub3" /><?php
							}?>
							<?php if( $res_bubbles4 ){
								?><input type="hidden" value="<?php echo $res_bubbles4; ?>" name="res_val_bub4" /><?php
							}?>
							<?php if( $res_bubbles5 ){
								?><input type="hidden" value="<?php echo $res_bubbles5; ?>" name="res_val_bub5" /><?php
							}?>
							<?php if( $res_bubbles6 ){
								?><input type="hidden" value="<?php echo $res_bubbles6; ?>" name="res_val_bub6" /><?php
							}?>
							<?php if( $res_bubbles7 ){
								?><input type="hidden" value="<?php echo $res_bubbles7; ?>" name="res_val_bub7" /><?php
							}?>
							<?php if( $res_bubbles8 ){
								?><input type="hidden" value="<?php echo $res_bubbles8; ?>" name="res_val_bub8" /><?php
							}?>
							<?php if( $res_bubbles9 ){
								?><input type="hidden" value="<?php echo $res_bubbles9; ?>" name="res_val_bub9" /><?php
							}?>
							<?php if( $res_bubbles10 ){
								?><input type="hidden" value="<?php echo $res_bubbles10; ?>" name="res_val_bub10" /><?php
							}?>
							<?php if( $res_bubbles11 ){
								?><input type="hidden" value="<?php echo $res_bubbles11; ?>" name="res_val_bub11" /><?php
							}?>
							<?php if( $res_bubbles12 ){
								?><input type="hidden" value="<?php echo $res_bubbles12; ?>" name="res_val_bub12" /><?php
							}?>
							<?php if( $res_bubbles13 ){
								?><input type="hidden" value="<?php echo $res_bubbles13; ?>" name="res_val_bub13" /><?php
							}?>
							<?php if( $res_bubbles14 ){
								?><input type="hidden" value="<?php echo $res_bubbles14; ?>" name="res_val_bub14" /><?php
							}?>
							<?php if( $res_bubbles15 ){
								?><input type="hidden" value="<?php echo $res_bubbles15; ?>" name="res_val_bub15" /><?php
							}?>
							<?php if( $res_bubbles16 ){
								?><input type="hidden" value="<?php echo $res_bubbles16; ?>" name="res_val_bub16" /><?php
							}?>
							<?php if( $res_bubbles17 ){
								?><input type="hidden" value="<?php echo $res_bubbles17; ?>" name="res_val_bub17" /><?php
							}?>
							<?php if( $res_bubbles18 ){
								?><input type="hidden" value="<?php echo $res_bubbles18; ?>" name="res_val_bub18" /><?php
							}?>
							<?php if( $res_bubbles19 ){
								?><input type="hidden" value="<?php echo $res_bubbles19; ?>" name="res_val_bub19" /><?php
							}?>
							<?php if( $res_bubbles20 ){
								?><input type="hidden" value="<?php echo $res_bubbles20; ?>" name="res_val_bub20" /><?php
							}?>

							<?php if( $res_strong1 ){
								?><input type="hidden" value="<?php echo $res_strong1; ?>" name="res_val_str1" /><?php
							}?>
							<?php if( $res_strong2 ){
								?><input type="hidden" value="<?php echo $res_strong2; ?>" name="res_val_str2" /><?php
							}?>
							<?php if( $res_strong3 ){
								?><input type="hidden" value="<?php echo $res_strong3; ?>" name="res_val_str3" /><?php
							}?>
							<?php if( $res_strong4 ){
								?><input type="hidden" value="<?php echo $res_strong4; ?>" name="res_val_str4" /><?php
							}?>
							<?php if( $res_strong5 ){
								?><input type="hidden" value="<?php echo $res_strong5; ?>" name="res_val_str5" /><?php
							}?>
							<?php if( $res_strong6 ){
								?><input type="hidden" value="<?php echo $res_strong6; ?>" name="res_val_str6" /><?php
							}?>
							<?php if( $res_strong7 ){
								?><input type="hidden" value="<?php echo $res_strong7; ?>" name="res_val_str7" /><?php
							}?>
							<?php if( $res_strong8 ){
								?><input type="hidden" value="<?php echo $res_strong8; ?>" name="res_val_str8" /><?php
							}?>
							<?php if( $res_strong9 ){
								?><input type="hidden" value="<?php echo $res_strong9; ?>" name="res_val_str9" /><?php
							}?>
							<?php if( $res_strong10 ){
								?><input type="hidden" value="<?php echo $res_strong10; ?>" name="res_val_str10" /><?php
							}?>
							<?php if( $res_strong11 ){
								?><input type="hidden" value="<?php echo $res_strong11; ?>" name="res_val_str11" /><?php
							}?>
							<?php if( $res_strong12 ){
								?><input type="hidden" value="<?php echo $res_strong12; ?>" name="res_val_str12" /><?php
							}?>
							<?php if( $res_strong13 ){
								?><input type="hidden" value="<?php echo $res_strong13; ?>" name="res_val_str13" /><?php
							}?>
							<?php if( $res_strong14 ){
								?><input type="hidden" value="<?php echo $res_strong14; ?>" name="res_val_str14" /><?php
							}?>
							<?php if( $res_strong15 ){
								?><input type="hidden" value="<?php echo $res_strong15; ?>" name="res_val_str15" /><?php
							}?>
							<?php if( $res_strong16 ){
								?><input type="hidden" value="<?php echo $res_strong16; ?>" name="res_val_str16" /><?php
							}?>
							<?php if( $res_strong17 ){
								?><input type="hidden" value="<?php echo $res_strong17; ?>" name="res_val_str17" /><?php
							}?>
							<?php if( $res_strong18 ){
								?><input type="hidden" value="<?php echo $res_strong18; ?>" name="res_val_str18" /><?php
							}?>
							<?php if( $res_strong19 ){
								?><input type="hidden" value="<?php echo $res_strong19; ?>" name="res_val_str19" /><?php
							}?>
							<?php if( $res_strong20 ){
								?><input type="hidden" value="<?php echo $res_strong20; ?>" name="res_val_str20" /><?php
							}?>

							<?php if( $res_soft1 ){
								?><input type="hidden" value="<?php echo $res_soft1; ?>" name="res_val_sof1" /><?php
							}?>
							<?php if( $res_soft2 ){
								?><input type="hidden" value="<?php echo $res_soft2; ?>" name="res_val_sof2" /><?php
							}?>
							<?php if( $res_soft3 ){
								?><input type="hidden" value="<?php echo $res_soft3; ?>" name="res_val_sof3" /><?php
							}?>
							<?php if( $res_soft4 ){
								?><input type="hidden" value="<?php echo $res_soft4; ?>" name="res_val_sof4" /><?php
							}?>
							<?php if( $res_soft5 ){
								?><input type="hidden" value="<?php echo $res_soft5; ?>" name="res_val_sof5" /><?php
							}?>


							<input type="hidden" value="<?php echo $res_extras; ?>" name="res_val_extras" />
							<input type="hidden" value="<?php echo $res_date; ?>" name="res_val_date" />
							<input type="hidden" value="<?php echo $res_total; ?>" name="res_val_total" />

							<ul class="reservations3-options-list form-list">

								<li>
									<div class="half_column">
										<!--<label>First name:</label>-->
										<input type="text" class="fp-styled-form-element" id="" name="res_val_name" placeholder="First name" required />
									</div>
									
									<div class="half_column_last">
										<!--<label>Last name:</label>-->
										<input type="text" class="fp-styled-form-element" id="" name="res_val_lastName" placeholder="Last name" required />
									</div>
								</li>

								<li>
									<div class="half_column">
										<!--<label>E-mail address:</label>-->
										<input type="text" class="fp-styled-form-element" id="" name="res_val_email" placeholder="Email adress" required />
									</div>
									<div class="half_column_last">
										<!--<label>Telephone:</label>-->
										<input type="text" class="fp-styled-form-element" id="" name="res_val_tel" placeholder="Telephone" required />
									</div>
								</li>

								<li>
									<div class="three_fourth_left">
										<!--<label>Streetname:</label>-->
										<input type="text" class="fp-styled-form-element" id="" name="res_val_street" placeholder="Streetname" required />
									</div>
									<div class="one_fourth_right">
										<!--<label>Number:</label>-->
										<input type="text" class="fp-styled-form-element" id="" name="res_val_housenr" placeholder="Number" required />
									</div>
								</li>

								<li>
									<div class="one_fourth_left">
										<!--<label>Postal code:</label>-->
										<input type="text" class="fp-styled-form-element" id="" name="res_val_postal" placeholder="Postal code" required />
									</div>
									<div class="three_fourth_right">
										<!--<label>City:</label>-->
										<input type="text" class="fp-styled-form-element" id="" name="res_val_city" placeholder="City" required />
									</div>
								</li>
								
							</ul>

							<input type="submit" value="Add to cart" class="btn btn-wide" />

						</form>
					</div>

				</div>

			</div>

		</div>

		<div class="half_column">

			<div class="main-events blocky">

				<?php
					$now = (date('U') - 86400) * 1000;
				$coming_events_args = array(
					'post_type' => 'event',
					'posts_per_page' => 3,
			            'meta_key' => 'event_date',
			            'meta_query' => array(
							array(
								'key'     => 'event_date',
								'value'   => $now,
								'compare' => '>='
							)
						),
			            'orderby' => 'meta_value',
			            'order' => 'ASC'
				);

				$coming_events = new WP_Query( $coming_events_args );

				if( $coming_events->have_posts() ) :
					while( $coming_events->have_posts() ) :
						$coming_events->the_post();
					
						get_template_part('content', 'event');

					endwhile;
				endif;
				?>

			</div>

			<div class="concept-nav-small">
					
				<?php 

				$event_cat_args = array(
				    'orderby'           => 'menu_order', 
				    'order'             => 'ASC',
				    'hide_empty'        => false, 
				    'exclude'           => array(), 
				    'exclude_tree'      => array(), 
				    'include'           => array(),
				    'number'            => '', 
				    'fields'            => 'all', 
				    'slug'              => '', 
				    'parent'            => '',
				    'hierarchical'      => true, 
				    'child_of'          => 0, 
				    'get'               => '', 
				    'name__like'        => '',
				    'description__like' => '',
				    'pad_counts'        => false, 
				    'offset'            => '', 
				    'search'            => '', 
				    'cache_domain'      => 'core'
				); 

				$event_cats = get_terms('event_concepts', $event_cat_args);

				if( $event_cats ) :
					?><ul class="main-concept-list-big"><?php

					foreach( $event_cats as $cat ) :

						$term_link = get_term_link( $cat );

						?><li><?php

							if( $cat->slug == 'i-love-feestpaleis' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept ilfp">
										<div class="event-concept-inner">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_ilfp.png" alt="i<3FeestPaleis" />
										</div>
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							if( $cat->slug == 'lovely-sundays' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept ls">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_ls.png" alt="Lovely Sundays" />
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							if( $cat->slug == 'after-work' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept aw">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_aw.png" alt="After Work" />
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							if( $cat->slug == 'level-3' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept l3">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_l3.png" alt="Lever 3" />
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							?></li><?php

					endforeach;

					?></ul><?php
				endif;

				?>

			</div>


			<div class="main-social">
			
				<?php get_template_part('content', 'social_list'); ?>

			</div>

		</div>

	</div>
</div>

<?php get_footer(); ?>