<?php
/*
Template name: Media
*/
?>

<?php get_header(); ?>

<div class="main">
	<div class="container container-padding">

		<div class="row">

			<div class="half_column_last clearfix">

				<div class="main-galleries">


				<ul class="grid-gallery-media">

					<?php
					$now = (date('U') - 86400) * 1000;



					$coming_events_args = array(
						'post_type' => 'gallery',
						'posts_per_page' => 16,

						'meta_key' => 'gallery_date', // name of custom field
						'orderby' => 'meta_value_num',
						'order' => 'DESC'
					);

					$coming_events = new WP_Query( $coming_events_args );

					if( $coming_events->have_posts() ) :
						while( $coming_events->have_posts() ) :
							$coming_events->the_post();

							get_template_part('content', 'gallery');

						endwhile;
					endif;
					
					wp_reset_query();

					?>

				</ul>

				</div>

				<a target="_blank" href="https://www.facebook.com/feestpaleis/photos_stream?tab=photos_albums" class="btn btn-wide btn-black btn-no-border" style="margin-top:45px; display:inline-block;">Discover more galleries on Facebook</a>


			</div>

			<div class="half_column">

				<div class="main-events">

					<?php
					$now = (date('U') - 86400) * 1000;
					$coming_events_args = array(
						'post_type' => 'event',
						'posts_per_page' => 3,
			            'meta_key' => 'event_date',
			            'meta_query' => array(
							array(
								'key'     => 'event_date',
								'value'   => $now,
								'compare' => '>='
							)
						),
			            'orderby' => 'meta_value',
			            'order' => 'ASC'
					);

					$coming_events = new WP_Query( $coming_events_args );

					if( $coming_events->have_posts() ) :
						while( $coming_events->have_posts() ) :
							$coming_events->the_post();

							get_template_part('content', 'event');

						endwhile;
					endif;
					
					wp_reset_query();

					?>

				</div>

				<div class="concept-nav-small">
					
					<?php 

					$event_cat_args = array(
					    'orderby'           => 'menu_order', 
				    	'order'             => 'ASC',
					    'hide_empty'        => false, 
					    'exclude'           => array( get_queried_object()->term_id ), 
					    'exclude_tree'      => array(), 
					    'include'           => array(),
					    'number'            => '', 
					    'fields'            => 'all', 
					    'slug'              => '', 
					    'parent'            => '',
					    'hierarchical'      => true, 
					    'child_of'          => 0, 
					    'get'               => '', 
					    'name__like'        => '',
					    'description__like' => '',
					    'pad_counts'        => false, 
					    'offset'            => '', 
					    'search'            => '', 
					    'cache_domain'      => 'core'
					); 

					$event_cats = get_terms('event_concepts', $event_cat_args);

					if( $event_cats ) :
						?><ul class="main-concept-list-small"><?php

						foreach( $event_cats as $cat ) :
							
							$term_link = get_term_link( $cat );

							?><li><?php

							if( $cat->slug == 'i-love-feestpaleis' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept ilfp">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_ilfp.png" alt="i<3FeestPaleis" />
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							if( $cat->slug == 'lovely-sundays' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept ls">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_ls.png" alt="Lovely Sundays" />
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							if( $cat->slug == 'after-work' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept aw">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_aw.png" alt="After Work" />
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							if( $cat->slug == 'level-3' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept l3">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_l3.png" alt="Lever 3" />
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							?></li><?php

						endforeach;

						?></ul><?php
					endif;

					?>

				</div>

				<div class="main-social">
			
					<?php get_template_part('content', 'social_list'); ?>

				</div>

			</div>

		</div>

	</div>

</div>

<?php get_footer(); ?>