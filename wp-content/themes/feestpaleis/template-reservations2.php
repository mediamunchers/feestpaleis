<?php
/*
Template name: Reservations Options
*/
?>

<?php 

if( isset($_POST['res1_persons']) ) :
	$persons = $_POST['res1_persons'];
elseif( isset($_POST['res_val_drink1']) ) :
	$persons = $_POST['res_val_drink1'];
else :
	wp_redirect( get_permalink( 15 ) );
	exit;
endif;

get_header(); ?>

<div class="main">
	<div class="container container-padding">

		<div class="half_column_last reservations-content">

			<div class="reservations-thumb">
				<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/reservation_vip_tafel.jpg" alt="" />
				<ul>
					<li><a href="<?php echo get_permalink(15); ?>" class="active">VIP Tafel</a></li>
					<li><a href="<?php echo get_permalink(391); ?>">Limo service</a></li>
					<li><a href="<?php echo get_permalink(450); ?>">Groepen</a></li>
				</ul>
			</div>

			<div class="reservations-container">

				<div class="reservation-total">
					Totaal: &euro; <span class="res_tot_val">...</span>
				</div>

				<div class="reservations-container-inner">

					<form id="res2_target" action="<?php echo get_permalink(146); ?>" method="post" class="res-stp3">


						<!-- ### HIDDEN FIELDS ### -->
						<input type="hidden" name="res2_persons" value="<?php echo $persons; ?>" class="res2_persons_placeholder" />
						<input type="hidden" name="res2_total" value="false" class="res2_price_placeholder" />




						<div class="res_container_block blocky">

							<h5 class="color-main">Vip tafel - <?php echo $persons; ?> personen</h5>
						    <?php
						    while ( have_posts() ) : the_post(); ?> 
					            <?php the_content(); ?> 
						    <?php
						    endwhile; 
						    wp_reset_query(); 
						    ?>

						</div>




						

						<div class="res_container_block res2_container_drinks">

							<div class="res2_container_drinks_rest">Resterend: &euro; <span class="res_rest_val">...</span></div>

							<h5 class="color-main">Selecteer uw dranken</h5>

							<div id="drinks_menu" class="blocky">

								<div class="drinks_menu_title">
									Bubbels
								</div>
								<div class="drinks_menu_container">

									<select name="res_val_bub1" id="res_drinks_bub1" class="ddslick-me drinks_bub1">
										<option
											value=""
											data-description="00.00">
											Selecteer uw drank
										</option>
									<?php

									$res_drinks_args = array(
										'post_type' => 'product',
										'showposts' => -1,
										// 'meta_key' => '_featured',  
										// 'meta_value' => 'yes',
										'product_cat' => 'bubbels',
							            'meta_key' => '_price',
							            'orderby' => 'meta_value_num',
							            'order' => 'ASC'
									);

									$res_drinks = new WP_query( $res_drinks_args );

									if( $res_drinks->have_posts() ) :
										while( $res_drinks->have_posts() ) :
											$res_drinks->the_post();

											$drink_thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
											$drink_price = get_post_meta( $post->ID, '_regular_price', true);

											?>


											<option
												value="<?php echo $post->ID; ?>"
												data-description="&euro; <?php echo $drink_price; ?>">
												<?php the_title(); ?>
											</option>
											

											<?php 
										endwhile;
									endif;

									wp_reset_query();

									?>

									</select>



									<select name="res_val_bub2" id="res_drinks_bub2" class="ddslick-me drinks_bub2">

										<option
											value=""
											data-description="00.00">
											Selecteer extra drank
										</option>

									<?php

									$res_drinks_args = array(
										'post_type' => 'product',
										'order' => 'ASC',
										'showposts' => -1,
										// 'meta_key' => '_featured',  
										// 'meta_value' => 'yes',
										'product_cat' => 'bubbels',
							            'meta_key' => '_price',
							            'orderby' => 'meta_value_num',
							            'order' => 'ASC'
									);

									$res_drinks = new WP_query( $res_drinks_args );

									if( $res_drinks->have_posts() ) :
										while( $res_drinks->have_posts() ) :
											$res_drinks->the_post();

											$drink_thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
											$drink_price = get_post_meta( $post->ID, '_regular_price', true);

											?>


											<option
												value="<?php echo $post->ID; ?>"
												data-description="&euro; <?php echo $drink_price; ?>">
												<?php the_title(); ?>
											</option>
											

											<?php 
										endwhile;
									endif;

									wp_reset_query();

									?>

									</select>



								</div>

								<div class="drinks_menu_title">
									Sterke Dranken
								</div>
								<div class="drinks_menu_container">
								<p>Alle sterke dranken worden geserveerd als bucket inclusief 6 frisdranken, behalve Peterman jenevers, Martini, Baileys en Boswandeling.</p>

									<select name="res_val_str1" id="res_drinks_str1" class="ddslick-me">
										<option
											value=""
											data-description="00.00">
											Selecteer uw drank
										</option>										
										<?php

										$res_drinks_args = array(
											'post_type' => 'product',
											'order' => 'ASC',
											'showposts' => -1,
											// 'meta_key' => '_featured',  
											// 'meta_value' => 'yes',
											'product_cat' => 'sterke-dranken',
							            'meta_key' => '_price',
							            'orderby' => 'meta_value_num',
							            'order' => 'ASC'
										);

										$res_drinks = new WP_query( $res_drinks_args );

										if( $res_drinks->have_posts() ) :
											while( $res_drinks->have_posts() ) :
												$res_drinks->the_post();

												$drink_thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
												$drink_price = get_post_meta( $post->ID, '_regular_price', true);

												?>


												<option
													value="<?php echo $post->ID; ?>"
													data-description="&euro; <?php echo $drink_price; ?>">
													<?php the_title(); ?>
												</option>
												

												<?php 
											endwhile;
										endif;

										wp_reset_query();

										?>

									</select>


									<select name="res_val_str2" id="res_drinks_str2" class="ddslick-me">

										<option
											value=""
											data-description="00.00">
											Selecteer extra drank
										</option>
										
										<?php

										$res_drinks_args = array(
											'post_type' => 'product',
											'order' => 'ASC',
											'showposts' => -1,
											// 'meta_key' => '_featured',  
											// 'meta_value' => 'yes',
											'product_cat' => 'sterke-dranken',
							            'meta_key' => '_price',
							            'orderby' => 'meta_value_num',
							            'order' => 'ASC'
										);

										$res_drinks = new WP_query( $res_drinks_args );

										if( $res_drinks->have_posts() ) :
											while( $res_drinks->have_posts() ) :
												$res_drinks->the_post();

												$drink_thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
												$drink_price = get_post_meta( $post->ID, '_regular_price', true);

												?>


												<option
													value="<?php echo $post->ID; ?>"
													data-description="&euro; <?php echo $drink_price; ?>">
													<?php the_title(); ?>
												</option>
												

												<?php 
											endwhile;
										endif;

										wp_reset_query();

										?>

									</select>

								</div>
								<!--
								<div class="drinks_menu_title">
									Frisdranken
								</div>
								<div class="drinks_menu_container">

									<select name="res_val_sof1" id="res_drinks_sof1" class="ddslick-me">
										<option
											value=""
											data-description="00.00">
											Selecteer uw drank
										</option>										
										<?php

										$res_drinks_args = array(
											'post_type' => 'product',
											'order' => 'ASC',
											'showposts' => -1,
											// 'meta_key' => '_featured',  
											// 'meta_value' => 'yes',
											'product_cat' => 'frisdranken'
										);

										$res_drinks = new WP_query( $res_drinks_args );

										if( $res_drinks->have_posts() ) :
											while( $res_drinks->have_posts() ) :
												$res_drinks->the_post();

												$drink_thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
												$drink_price = get_post_meta( $post->ID, '_regular_price', true);

												?>


												<option
													value="<?php echo $post->ID; ?>"
													data-description="&euro; <?php echo $drink_price; ?>">
													<?php the_title(); ?>
												</option>
												

												<?php 
											endwhile;
										endif;

										wp_reset_query();

										?>

									</select>


									<select name="res_val_sof2" id="res_drinks_sof2" class="ddslick-me">

										<option
											value=""
											data-description="00.00">
											Selecteer extra drank
										</option>
										
										<?php

										$res_drinks_args = array(
											'post_type' => 'product',
											'order' => 'ASC',
											'showposts' => -1,
											// 'meta_key' => '_featured',  
											// 'meta_value' => 'yes',
											'product_cat' => 'frisdranken'
										);

										$res_drinks = new WP_query( $res_drinks_args );

										if( $res_drinks->have_posts() ) :
											while( $res_drinks->have_posts() ) :
												$res_drinks->the_post();

												$drink_thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
												$drink_price = get_post_meta( $post->ID, '_regular_price', true);

												?>


												<option
													value="<?php echo $post->ID; ?>"
													data-description="&euro; <?php echo $drink_price; ?>">
													<?php the_title(); ?>
												</option>
												

												<?php 
											endwhile;
										endif;

										wp_reset_query();

										?>

									</select>

								</div>
								-->
							</div>
							

						</div>

						<!--
						<div class="res_container_block res_container_extras blocky">

							<h5 class="color-main">Selecteer uw extra's</h5>

							<select name="res_extras" id="res_extras2">
								<option value="none">Geen extra's</option>
								<option value="shuttle">Shuttle (€50 sup.)</option>
								<option value="limo">Limo (€295 sup.)</option>
								<option value="hotel">Hotel (€240 sup.)</option>
							</select>

						</div>
						-->

						<div class="res_container_block blocky">

							<h5 class="color-main">Selecteer de gewenste datum</h5>

							<input type="text" id="res_date" class="fp-styled-form-element" name="res_val_date" value="" placeholder="Selecteer de gewenste datum..." />

						</div>

						<div class="res_errors">
						</div>


						<input type="button" value="Doorgaan" class="btn btn-wide btn-submit" />

					</form>

				</div>

			</div>

		</div>

		<div class="half_column">

			<div class="main-events blocky">

				<?php
					$now = (date('U') - 86400) * 1000;
				$coming_events_args = array(
					'post_type' => 'event',
					'posts_per_page' => 3,
			            'meta_key' => 'event_date',
			            'meta_query' => array(
							array(
								'key'     => 'event_date',
								'value'   => $now,
								'compare' => '>='
							)
						),
			            'orderby' => 'meta_value',
			            'order' => 'ASC'
				);

				$coming_events = new WP_Query( $coming_events_args );

				if( $coming_events->have_posts() ) :
					while( $coming_events->have_posts() ) :
						$coming_events->the_post();
					
						get_template_part('content', 'event');

					endwhile;
				endif;

				wp_reset_query();
				?>

			</div>

			<div class="concept-nav-small">
					
				<?php 

				$event_cat_args = array(
				    'orderby'           => 'menu_order', 
				    'order'             => 'ASC',
				    'hide_empty'        => false, 
				    'exclude'           => array(), 
				    'exclude_tree'      => array(), 
				    'include'           => array(),
				    'number'            => '', 
				    'fields'            => 'all', 
				    'slug'              => '', 
				    'parent'            => '',
				    'hierarchical'      => true, 
				    'child_of'          => 0, 
				    'get'               => '', 
				    'name__like'        => '',
				    'description__like' => '',
				    'pad_counts'        => false, 
				    'offset'            => '', 
				    'search'            => '', 
				    'cache_domain'      => 'core'
				); 

				$event_cats = get_terms('event_concepts', $event_cat_args);

				if( $event_cats ) :
					?><ul class="main-concept-list-big"><?php

					foreach( $event_cats as $cat ) :

						$term_link = get_term_link( $cat );

						?><li><?php

							if( $cat->slug == 'i-love-feestpaleis' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept ilfp">
										<div class="event-concept-inner">
											<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_ilfp.png" alt="i<3FeestPaleis" />
										</div>
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							if( $cat->slug == 'lovely-sundays' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept ls">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_ls.png" alt="Lovely Sundays" />
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							if( $cat->slug == 'after-work' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept aw">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_aw.png" alt="After Work" />
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							if( $cat->slug == 'level-3' ) :
								?><a href="<?php echo $term_link; ?>">
									<div class="event-concept l3">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/images/theme/concept_l3.png" alt="Lever 3" />
									</div>
									<span class="event-concept-mask"></span>
								</a><?php
							endif;

							?></li><?php

					endforeach;

					?></ul><?php
				endif;

				?>

			</div>

			<div class="main-social">
			
				<?php get_template_part('content', 'social_list'); ?>

			</div>

		</div>

	</div>
</div>

<?php get_footer(); ?>